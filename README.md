# LaTíaMarta

### Password validator
http://www.passay.org/

### Iconos utilizados
https://fontawesome.com/v4.7.0/icons/

### Configuración de latiamarta como servicio de linux para deploy

* Crear un archivo en -> /lib/systemd/system/latiamarta.service

* Contenido: 

```sh
[Unit]
Description=Aplicacion latiamarta.com
After=postgresql.service network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/bin/bash /home/proyectos/deploy.sh

[Install]
WantedBy=multi-user.target
```

* Para que inicie con el sistema: 

```sh
sudo systemctl enable latiamarta
```

* Para iniciarlo:

```sh
sudo systemctl start latiamarta
```

### Para desplegar

* Actualizar properties productivas en latiamarta -> properties -> latiamarta.properties.

* Los scripts de Flyway se ejecutan solos.

* Correr:
```sh
systemctl restart latiamarta
```

```sh
systemctl status latiamarta
``` 
Esto último si es que se quiere ver el estado del deploy.

  
