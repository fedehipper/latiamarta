Vue.component("boton-deshacer", {
    props:["activoId"],
    data: function() {
        return {
            modalId: "deshacer" + this.activoId
        };
    },
    methods: {
        deshacer() {
            this.$emit("deshacerMovimientoHoy");
        },
        verModal() {
            $("#" + this.modalId).modal();
        }
    },
    template: `
        <div>
            <modal-confirmacion-deshacer :modalId="modalId" @confirmarDeshacer="deshacer()"/>
            <button class="btn btn-block btn-primary boton-acciones" @click="verModal">Deshacer</button>
        </div>
    `
});