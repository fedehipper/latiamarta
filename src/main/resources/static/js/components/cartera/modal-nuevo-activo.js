Vue.component("modal-alta-activo", {
    props: ["tipoMonedas", "criptomonedasPorSimbolo"],
    data: function () {
        return {
            nombreActivo: "",
            montoInvertido: "",
            moneda: "",
            esCriptomoneda: false,
            simboloCriptomoneda: "",
            criptomonedaSeleccionada: "",
            mensajeSeleccionarCriptomoneda: false,
            mensajeSinResultadosParaBusqueda: false,
            criptomonedaPrimeraInversion: false
        };
    },
    mounted() {
        this.resetear();
    },
    methods: {
        confirmar() {
            if (this.esCriptomoneda && !this.criptomonedaSeleccionada) {
                this.mensajeSeleccionarCriptomoneda = true;
            } else {
                var activo = {
                    nombre: this.nombreActivo,
                    monto: this.montoInvertido,
                    moneda: this.moneda,
                    criptomonedaId: this.criptomonedaSeleccionada.id,
                    criptomonedaImagenUrl: this.criptomonedaSeleccionada.imagen,
                    criptomonedaPrimeraInversion: this.criptomonedaPrimeraInversion
                };
                this.$emit("activoNuevo", activo);
                this.nombreActivo = "";
                this.montoInvertido = "";
                $("#altaActivo").modal("hide");
                this.resetear();
            }
        },
        buscarCriptoMonedasParaSimboloIngresado() {
            this.$emit("simboloCriptomoneda", this.simboloCriptomoneda);
        },
        vaciarCriptoMonedasParaSimboloIngresado() {
            this.$emit("simboloCriptomoneda", "");
        },
        resaltarAlSeleccionar(criptomoneda) {
            if (this.criptomonedaSeleccionada === criptomoneda) {
                return "text-success border-success";
            }
        },
        asignarCriptomonedaSeleccionada(criptomoneda) {
            this.criptomonedaSeleccionada = criptomoneda;
            this.mensajeSeleccionarCriptomoneda = false;
        },
        resetear() {
            this.nombreActivo = "";
            this.montoInvertido = "";
            this.moneda = "ARS";
            this.esCriptomoneda = false;
            this.simboloCriptomoneda = "";
            this.criptomonedaSeleccionada = "";
            this.mensajeSeleccionarCriptomoneda = false;
            this.mensajeSinResultadosParaBusqueda = false;
            this.criptomonedaPrimeraInversion = false;
            this.vaciarCriptoMonedasParaSimboloIngresado();
        }
    },
    watch: {
        esCriptomoneda() {
            if (this.esCriptomoneda) {
                if (this.simboloCriptomoneda) {
                    this.$emit("simboloCriptomoneda", this.simboloCriptomoneda);
                }
            } else {
                this.vaciarCriptoMonedasParaSimboloIngresado();
            }
            this.criptomonedaPrimeraInversion = false;
        },
        criptomonedasPorSimbolo() {
            if (this.criptomonedasPorSimbolo && this.criptomonedasPorSimbolo.length === 0) {
                this.mensajeSinResultadosParaBusqueda = true;
            } else {
                this.mensajeSinResultadosParaBusqueda = false;
            }
        }
    },
    created() {
        this.moneda = "ARS";
    },
    template: `
        <div class="modal fade" id="altaActivo" tabindex="-1" >
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" @keyup.esc="resetear">

                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo activo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="resetear">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form @submit="confirmar" @submit.prevent>
                        <div class="modal-body">
                                <div class="row">
                                    <div class="col mb-2">
                                        <label>Nombre</label>
                                        <input 
                                            type="text"
                                            maxlength="15"
                                            class="form-control"
                                            required
                                            v-model="nombreActivo">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="formularioMonedas">Moneda</label>
                                        <select class="form-control" v-model="moneda">
                                            <option 
                                                v-for="tipoMoneda in tipoMonedas" :value="tipoMoneda">{{tipoMoneda}}
                                            </option>
                                        </select>
                                    </div>    
                                </div>
                                <div class="row"> 
                                    <div class="col mt-3" id="inputMonto">
                                        <label>Monto</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input 
                                                type="number" step=0.01 
                                                class="form-control" 
                                                required
                                                min="0" max="999999999999999"
                                                v-model="montoInvertido">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <label>
                                        ¿Es una criptomoneda?
                                    </label>
                                    <input 
                                        class="form-check-input fa-clickable ml-5" 
                                        v-model="esCriptomoneda" 
                                        name="gridRadios"
                                        type="checkbox">
                                    <span class="ml-1" v-if="esCriptomoneda">Si</span>
                                    <span class="ml-1" v-else>No</span>
                                </div>
                                <div v-if="esCriptomoneda">
                                    <div>
                                        <label>Busca la criptomoneda por su símbolo:</label>
                                        <input 
                                            class="form-control"
                                            type="text"
                                            maxlength="15"
                                            v-model="simboloCriptomoneda">
                                        <button type="button" class="col btn btn-primary mt-3" @click="buscarCriptoMonedasParaSimboloIngresado">Buscar</button>
                                    </div>
                                </div>
                                <div v-if="mensajeSinResultadosParaBusqueda" class="text-info mt-3">No se encuentra el símbolo que ingresaste...</div>
                                <div v-else class="mt-3 fa-clickable">
                                    <div 
                                        class="card mt-2 p-2" 
                                        :class="resaltarAlSeleccionar(criptomonedaPorSimbolo)" 
                                        v-for="criptomonedaPorSimbolo in criptomonedasPorSimbolo" 
                                        @click="asignarCriptomonedaSeleccionada(criptomonedaPorSimbolo)"   
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div class="row">
                                                <div class="ml-3">
                                                    <img :src="criptomonedaPorSimbolo.imagen" width="30"/> 
                                                </div>
                                                <div class="ml-2 mt-1">
                                                    <span>{{criptomonedaPorSimbolo.name}} ({{criptomonedaPorSimbolo.symbol}})</span>
                                                </div>
                                            </div>
                                            <div v-if="criptomonedaSeleccionada.id === criptomonedaPorSimbolo.id" class="mt-1 mr-1">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-if="mensajeSeleccionarCriptomoneda" class="text-danger mt-2">Selecciona una criptomoneda.</div>
                                </div>
                                <div v-if="esCriptomoneda" class="mt-3">
                                    <label>
                                        ¿Es tu primera inversión en esta criptomoneda?
                                    </label>
                                    <input
                                        class="form-check-input fa-clickable ml-5"
                                        v-model="criptomonedaPrimeraInversion" 
                                        name="gridRadios"
                                        type="checkbox">
                                    <span class="ml-1" v-if="criptomonedaPrimeraInversion">Si</span>
                                    <span class="ml-1" v-else>No</span>
                                </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-outline-secondary mr-2" data-dismiss="modal" @click="resetear">Cancelar</button>
                            <button type="submit" class="btn btn-sm btn-primary ml-2">Aceptar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        `

});
