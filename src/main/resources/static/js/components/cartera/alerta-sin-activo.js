Vue.component("alertaSinActivo", {
    template: `
        <div class="alert alert-info text-center mt-3" role="alert">
            En esta sección tenes la libertad de cargar tus inversiones, comenzá con la primera.
        </div>
`
});