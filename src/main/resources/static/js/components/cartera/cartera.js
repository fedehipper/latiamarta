Vue.component("cartera", {
    data: function () {
        return {
            mostrarSpinner: true,
            graficoDona: null,
            cartera: [],
            porcentajes: [],
            mostrarMensajeSinActivos: false,
            toast: {
                mensaje: "",
                color: ""
            },
            tipoMonedas: [],
            detalles: [],
            movimientos: [],
            activoSeleccionado: "",
            criptomonedasPorSimbolo: [],
            visualizacionTotal: ""
        };
    },
    methods: {
        obtenerDetalles() {
            axios.get("/api/detalle-activo")
                    .then(response => this.detalles = response.data);
        },
        obenerTipoMonedas() {
            axios.get("/api/tipo-moneda")
                    .then(response => this.tipoMonedas = response.data);
        },
        rescateTotalInversion(activoId) {
            axios.delete("/api/activo/" + activoId)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Rescataste parte de tu inversión.", "bg-success");
                    });
        },
        crearActivo(activo) {
            axios.post("/api/activo", activo)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Tu inversión se creó correctamente.", "bg-success");
                    });
        },
        rescateParcial(activoId, monto) {
            axios.post("/api/movimiento/rescate?activoId=" + activoId + "&" + "monto=" + monto)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Rescataste parte de tu inversión.", "bg-success");
                    })
                    .catch(error => {
                        this.lanzarAlerta(error.response.data, "bg-danger");
                    });
        },
        lanzarAlerta(mensaje, color) {
            this.toast.mensaje = mensaje;
            this.toast.color = color;
            $(".toast").toast("show");
        },
        invertirSobreVigente(activoId, monto, montoRecibido) {
            axios.post("/api/movimiento/inversion-nueva?activoId=" + activoId + "&monto=" + monto + "&esRecibido=" + montoRecibido)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Acabas de invertir sobre tu activo.", "bg-success");
                    })
                    .catch(error => this.lanzarAlerta(error.response.data, "bg-danger"));
        },
        obtenerCartera() {
            axios.get("/api/cartera")
                    .then(response => {
                        this.cartera = response.data;
                        if (this.cartera.length === 0) {
                            this.mostrarMensajeSinActivos = true;
                        } else {
                            this.mostrarMensajeSinActivos = false;
                        }
                        this.ocultarSpinner();
                    })
                    .then(() => this.buscarVisualizacionTotal())
                    .then(() => this.obenerTipoMonedas())
                    .then(() => this.obtenerDetalles())
                    .then(() => this.obtenerPorcentajes());
        },
        obtenerPorcentajes() {
            axios.get("/api/porcentaje")
                    .then(response => {
                        this.porcentajes = response.data;
                        if (this.porcentajes.length > 0) {
                            this.graficar(this.porcentajes);
                        }
                    });
        },
        deshacerMovimientoHoy(activoId) {
            axios.delete("/api/movimiento/activo/" + activoId)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Se deshiso el cambio en tu movimiento.", "bg-success");
                    })
                    .catch(error => this.lanzarAlerta(error.response.data, "bg-danger"));
        },
        actualizarUnActivo(idActivo, montoActualizado) {
            var activoDiario = {
                id: idActivo,
                monto: montoActualizado
            };
            axios.post("/api/movimiento", activoDiario)
                    .then(() => {
                        this.obtenerCartera();
                        this.lanzarAlerta("Se actualizó el monto de tu activo.", "bg-success");
                    })
                    .catch(error => this.lanzarAlerta(error.response.data, "bg-danger"));
        },
        buscarMovimientosPorActivoId(activoSeleccionado) {
            this.activoSeleccionado = activoSeleccionado;
            axios.get("/api/movimiento/activo/" + this.activoSeleccionado.id)
                    .then(response => {
                        this.movimientos = response.data;
                        $("#modalMovimientosId").modal();
                    });
        },
        buscarCriptomonedasPorSimbolo(simbolo) {
            if (!simbolo) {
                this.criptomonedasPorSimbolo = null;
            } else {
                axios.get("/api/criptomoneda/" + simbolo)
                        .then(response => {
                            this.criptomonedasPorSimbolo = response.data;
                        })
                        .catch(error => this.lanzarAlerta("No se encontró el símbolo", "bg-danger"));
            }
        },
        buscarVisualizacionTotal() {
            axios.get("/api/visualizacion-total")
                    .then(response => {
                        this.visualizacionTotal = response.data;
                    });
        },
        modificarVisualizacionTotal() {
            axios.put("/api/visualizacion-total")
                    .then(response => {
                        this.visualizacionTotal = response.data;
                    });
        },
        obtenerColor() {
            return "#" + ((1 << 24) * Math.random() | 0).toString(16);
        },
        graficar(porcentajes) {
            var porcentajesActivo = porcentajes.map(porcentaje => porcentaje.porcentajeActivo);
            var nombresActivo = porcentajes.map(porcentaje => porcentaje.nombreActivo);

            var colores = [];
            for (var i = 0; i < porcentajes.length; i++) {
                colores.push(this.obtenerColor());
            }
            if (this.graficoDona !== null) {
                this.graficoDona.destroy();
            }
            this.graficoDona = new Chart(document.getElementById("porcentajesActivos"), {
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            title: function (tooltipItem, data) {
                                return data['labels'][tooltipItem[0]['index']];
                            },
                            label: function (tooltipItem, data) {
                                var dataset = data['datasets'][0];
                                var percent = Math.round((dataset['data'][tooltipItem['index']]))
                                return percent + ' %';
                            }
                        },
                        backgroundColor: '#fff',
                        titleFontSize: 12,
                        titleFontColor: '#000',
                        bodyFontColor: '#000',
                        bodyFontSize: 14
                    }
                },
                type: "doughnut",
                data: {
                    datasets: [{
                            data: porcentajesActivo,
                            backgroundColor: colores
                        }],
                    labels: nombresActivo
                }
            });
        },
        ocultarSpinner() {
            this.mostrarSpinner = false;
        },
        cargarTransaccionHistorica(activoId, transaccion) {
            axios.post("/api/transaccion-historica/" + activoId, transaccion)
                    .then(response => this.lanzarAlerta("Transacción cargada con éxito.", "bg-success"))
                    .catch(error => this.lanzarAlerta(error.response.data, "bg-danger"));
        }
    },
    mounted() {
        this.obtenerCartera();
    },
    template: `
        
       <div>    
            <spinner v-if="mostrarSpinner"/>
            <div v-else>
                <div class="col" v-if="mostrarMensajeSinActivos">
                    <alertaSinActivo/>
                    <modal-alta-activo :criptomonedasPorSimbolo="criptomonedasPorSimbolo" @simboloCriptomoneda="buscarCriptomonedasPorSimbolo" @activoNuevo="crearActivo" :tipoMonedas="tipoMonedas"/>
                    <div class="text-center"> 
                        <boton-comenzar-a-invertir/>
                    </div>
                </div>
                <div v-else>
                    <div>
                        <modal-alta-activo :criptomonedasPorSimbolo="criptomonedasPorSimbolo" @simboloCriptomoneda="buscarCriptomonedasPorSimbolo" @activoNuevo="crearActivo" :tipoMonedas="tipoMonedas"/>
                        <modal-movimientos :movimientos="movimientos" :nombreActivo="activoSeleccionado.nombre" :moneda="activoSeleccionado.moneda"/>

                        <div class="pt-3 pr-3 pl-3">
                            <activos 
                                class="col pl-0 pr-0 pr-sm-3 pl-sm-3" 
                                :detalles="detalles" 
                                :montoOculto="visualizacionTotal.montoOculto"
                                @modificarVisualizacionTotal="modificarVisualizacionTotal"
                            />
                        </div>
                                                
                        <div class="m-3 lista-activos">
                            <hr/>
                            <div class="mb-3">
                                <boton-nuevo-activo class="boton-mobile-accion"/>
                            </div>
                            <div class="row d-flex flex-row">
                                <activo
                                    class="col-md-6"
                                    v-for="activo in cartera"
                                    :activo="activo" 
                                    :key="cartera.id"
                                    :montoOculto="visualizacionTotal.montoOculto"
                                    @confirmarActivo="actualizarUnActivo" 
                                    @rescateTotalInversion="rescateTotalInversion" 
                                    @invertirSobreVigente="invertirSobreVigente"
                                    @deshacerMovimientoHoy="deshacerMovimientoHoy"
                                    @rescateParcial="rescateParcial"
                                    @buscarMovimientosPorActivoId="buscarMovimientosPorActivoId"
                                    @transaccionHistorica="cargarTransaccionHistorica"
                                    @refrescarCartera="obtenerCartera"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <mensaje-accion :mensaje="toast.mensaje" :color="toast.color"/>
        </div>
    `
});

