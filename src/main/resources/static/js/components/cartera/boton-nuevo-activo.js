Vue.component("boton-nuevo-activo", {
    methods: {
        verModal() {
            $("#altaActivo").modal();
        }
    },
    template: `
        <div class="boton-nueva-inversion">
            <button class="btn btn-primary boton-mobile-accion" @click="verModal" data-target="#altaActivo">Nuevo activo</button>
        </div>
    `

});