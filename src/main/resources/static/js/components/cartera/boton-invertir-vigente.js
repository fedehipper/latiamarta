Vue.component("boton-invertir-vigente", {
    props: ["idActivo"],
    data: function () {
        return {
            accion: "",
            titulo: "",
            placeholder: "",
            tituloInput: ""
        };
    },
    created() {
        this.accion = "inversion";
        this.titulo = "Invertir";
        this.placeholder = "Monto a adicionar al vigente";
        this.tituloInput = "Monto a adicionar";
    },
    methods: {
        invertirSobreVigente(idActivo, montoActualizado, montoRecibido) {
            this.$emit("invertirSobreVigente", idActivo, montoActualizado, montoRecibido);
        },
        verModal() {
            $("#" + this.idActivo + this.accion).modal("show");
        }
    },
    template: `
        <div>
            <button class="btn btn-primary btn-block boton-acciones" @click="verModal">Invertir</button>
            <actualizacion-monto 
                @actualizarMonto="invertirSobreVigente" 
                :accion="accion"
                :titulo="titulo"
                :placeholder="placeholder"
                :tituloInput="tituloInput"
                :idActivo="idActivo"/>
        </div>
    `
});

