Vue.component("activo", {
    props: ["activo", "montoOculto"],
    data: function () {
        return {
            enEdicion: false,
            activoSeleccionado: false
        };
    },
    directives: {
        focus: {
            inserted: function (el) {
                el.focus();
                el.select();
            }
        }
    },
    methods: {
        buscarMovimientosPorActivoId() {
            this.$emit("buscarMovimientosPorActivoId", this.activo);
        },
        rescateTotalInversion(activoId) {
            this.$emit("rescateTotalInversion", activoId);
        },
        rescateParcial(activoId, monto) {
            this.$emit("rescateParcial", activoId, monto);
        },
        invertirSobreVigente(activoId, monto, montoRecibido) {
            this.enEdicion = false;
            this.$emit("invertirSobreVigente", activoId, monto, montoRecibido);
        },
        actualizarMonto(activoId, montoActualizado) {
            this.enEdicion = false;
            this.$emit("confirmarActivo", activoId, montoActualizado);
        },
        deshacerMovimientoHoy() {
            this.$emit("deshacerMovimientoHoy", this.activo.id);
        },
        colorPorEstadoGanancia(ganancia) {
            if (ganancia > 0) {
                return "text-success";
            }
            if (ganancia < 0) {
                return "text-danger";
            }
        },
        cancelar() {
            this.enEdicion = false;
        },
        desplegarModalCargaHistoriaTransacciones() {
            $("#" + this.activo.id + 'carga-historial').modal();
        },
        cargarTransaccionHistorica(activoId, transaccion) {
            this.$emit("transaccionHistorica", activoId, transaccion);
        },
        refrescarCartera() {
            this.$emit("refrescarCartera");
        }
    },
    mounted() {
        const self = this;
        $("#collapse" + self.activo.id).on('show.bs.collapse', function () {
            self.activoSeleccionado = true;
        });
        $("#collapse" + self.activo.id).on('hide.bs.collapse', function () {
            self.activoSeleccionado = false;
        });
    },
    template: `
            <div class="accordion pb-3" id="accordionActivos">
                <modal-carga-historia-transacciones 
                    :idActivo="activo.id" 
                    @refrescarCartera="refrescarCartera"
                    @transaccionHistorica="cargarTransaccionHistorica"/>
                <div class="card card-activo border-light">
                    <div class="card-header bg-light p-3" :id="'heading' + activo.id">
                      <h2 class="mb-0">
                        <button 
                            class="btn btn-block text-left collapse-activo collapsed pt-0 pb-0 pl-3 pr-3" 
                            type="button" 
                            data-toggle="collapse" 
                            :data-target="'#collapse' + activo.id" 
                            aria-expanded="false" 
                            :aria-controls="'collapse' + activo.id">
    
                            <div class="row d-flex justify-content-between">
                                <div v-if="activo.criptomonedaImagenUrl" class="row">
                                    <div  class="col pr-2">
                                        <img :src="activo.criptomonedaImagenUrl" width="30"/>
                                    </div>
                                    <div class="pl-0 mt-1">
                                        <h5 class="titulo-activo m-0"><b>{{activo.nombre}}</b></h5>
                                    </div>
                                </div>
                                <div v-else>
                                    <h5 class="titulo-activo m-0"><b>{{activo.nombre}}</b></h5>
                                </div>
                                <i v-if="activoSeleccionado" class="fa fa-chevron-up"></i><i v-else class="fa fa-chevron-down"></i>
                            </div>
                            <div class="row">
                                <span v-if="!montoOculto" class="m-0">{{activo.moneda}} ***</span>
                                <span v-else class="m-0">{{activo.moneda}} {{activo.montoActual}}</span>
                                <span :class=colorPorEstadoGanancia(activo.gananciaParcial) class="ml-2"> (
                                    <i v-if="activo.gananciaParcial > 0" class="fa fa-arrow-up"></i>
                                    <i v-if="activo.gananciaParcial < 0" class="fa fa-arrow-down"></i> 
                                    <span v-if="!montoOculto">{{activo.moneda}} ***</span>
                                    <span v-else>{{activo.moneda}} {{activo.gananciaParcial}}</span>
                                )
                                </span>
                            </div>
                        </button>
                      </h2>
                    </div>

                    <div :id="'collapse' + activo.id" class="collapse" :aria-labelledby="'heading' + activo.id" data-parent="#accordionActivos">
                        <div class="card-body pt-0 pb-3 p-3">
                        
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between pl-0 pr-0">
                                    <span>Monto actualizado </span>
                                    <span v-if="!montoOculto">{{activo.moneda}} ***</span>
                                    <span v-else>{{activo.moneda}} {{activo.montoActual}}</span>
                                </li>
                                
                                <li class="list-group-item d-flex justify-content-between pl-0 pr-0">
                                    <span>Rendimiento parcial </span>
                                    <span v-if="!montoOculto" :class=colorPorEstadoGanancia(activo.gananciaParcial)>
                                        {{activo.moneda}} ***
                                    </span>
                                    <span v-else :class=colorPorEstadoGanancia(activo.gananciaParcial)>
                                        {{activo.moneda}} {{activo.gananciaParcial}}
                                    </span>
                                </li>
    
                                <li class="list-group-item d-flex justify-content-between pl-0 pr-0">
                                    <span>Rendimiento total </span>
                                    <span v-if="!montoOculto" :class=colorPorEstadoGanancia(activo.gananciaTotal)>
                                        {{activo.moneda}} ***
                                    </span>
                                    <span v-else :class=colorPorEstadoGanancia(activo.gananciaTotal)>
                                        {{activo.moneda}} {{activo.gananciaTotal}}
                                    </span>
                                </li>
    
                                <li v-if="activo.criptoMoneda" class="list-group-item d-flex justify-content-between pl-0 pr-0">
                                    <span>Valor medio</span>
                                    <span v-if="activo.valorMedio">
                                        <span v-if="!montoOculto">{{activo.moneda}} ***</span>
                                        <span v-else>{{activo.moneda}} {{activo.valorMedio}}</span>
                                    </span>
                                    <div v-else>
                                        <button 
                                            class="btn btn-sm btn-primary" 
                                            @click="desplegarModalCargaHistoriaTransacciones">
                                            Obtener el valor medio
                                        </button>
                                    </div>
                                </li>
                            </ul>

                            <div class="row pl-3 d-flex justify-content-center">
                                <boton-editar class="boton-mobile-accion pr-3 pt-3" @actualizarMonto="actualizarMonto" :idActivo="activo.id" :montoActual="activo.montoActual"/>
                                <boton-deshacer class="boton-mobile-accion pr-3 pt-3" @deshacerMovimientoHoy="deshacerMovimientoHoy" :activoId="activo.id"/>
                                <boton-finalizar class="boton-mobile-accion pr-3 pt-3" @rescateTotalInversion="rescateTotalInversion" @rescateParcial="rescateParcial" :idActivo="activo.id"/>
                                <boton-invertir-vigente class="boton-mobile-accion pr-3 pt-3" @invertirSobreVigente="invertirSobreVigente" :idActivo="activo.id"/>
                                <div class="boton-mobile-accion pr-3 pt-3">
                                    <button class="btn btn-primary btn-block boton-acciones" @click="buscarMovimientosPorActivoId">Historial</button>
                                </div>
                            </div>
                        
                        </div>
                    </div>

                </div>
            </div>
    `
});