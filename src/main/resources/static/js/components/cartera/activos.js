Vue.component("activos", {
    props: ["detalles", "montoOculto"],
    data: function () {
        return {
            monedaSeleccionada: '',
            monedasDisponibles: [],
            detalleSeleccionado: ''
        };
    },
    methods: {
        colorPorEstadoGanancia(ganancia) {
            if (ganancia > 0) {
                return "text-success";
            }
            if (ganancia < 0) {
                return "text-danger";
            }
        },
        cambiarDetalleSeleccionado() {
            return this.detalles.find(detalle => this.monedaSeleccionada === detalle.tipoMoneda);
        },
        modificarVisualizacionTotal() {
            this.$emit("modificarVisualizacionTotal");
        }
    },
    watch: {
        detalles() {
            this.detalleSeleccionado = this.detalles[0];
            this.monedaSeleccionada = this.detalles[0].tipoMoneda;
            this.monedasDisponibles = this.detalles.map(detalle => detalle.tipoMoneda);
        },
        monedaSeleccionada() {
            this.detalleSeleccionado = this.cambiarDetalleSeleccionado();
        }
    },
    template: `
            <div>
                <div class="row">
                    <div class="info-general">
                        <div v-if="detalles.length > 1" class="pb-0 pt-3 pl-3 pr-3">
                            <select class="form-control" v-model="monedaSeleccionada">
                                <option 
                                    v-for="tipoMoneda in monedasDisponibles" :value="tipoMoneda">{{tipoMoneda}}
                                </option>
                            </select>   
                        </div>
                        <div class="p-3">
                            <div>
                                <div class="d-flex">
                                    <h2 v-if="!montoOculto" class="mb-0">
                                        {{detalleSeleccionado.tipoMoneda === 'ARS' ? 'ARS' : detalleSeleccionado.tipoMoneda}} ***
                                    </h2>
                                    <h2 v-else class="mb-0">
                                        {{detalleSeleccionado.tipoMoneda === 'ARS' ? 'ARS' : detalleSeleccionado.tipoMoneda}} {{detalleSeleccionado.montoActual}}
                                    </h2>
                                    <div 
                                        v-if="montoOculto" 
                                        class="pl-3 mt-2" 
                                        title="Activar o desactivar modo discreto"
                                        @click="modificarVisualizacionTotal"
                                    >
                                        <i class="fa fa-eye fa-clickable"></i>
                                    </div>
                                    <div 
                                        v-else 
                                        class="pl-3 mt-2" 
                                        title="Activar o desactivar modo discreto"
                                        @click="modificarVisualizacionTotal"
                                    >
                                        <i class="fa fa-eye-slash fa-clickable"></i>
                                    </div>
                                </div>
                                <p class="tamanio-texto-mini mb-0">balance total</p>
                            </div>
                            <div class="d-flex justify-content-between pb-3 pt-3" :class="colorPorEstadoGanancia(detalleSeleccionado.gananciaParcial)">
                                <h5 class="mb-0" :class="colorPorEstadoGanancia(detalleSeleccionado.gananciaParcial)">
                                    <span v-if="detalleSeleccionado.gananciaParcial > 0"> 
                                        <span v-if="!montoOculto">( <i class="fa fa-arrow-up"></i> {{detalleSeleccionado.tipoMoneda}} *** )</span>
                                        <span v-else>( <i class="fa fa-arrow-up"></i> {{detalleSeleccionado.tipoMoneda}} {{detalleSeleccionado.gananciaParcial}} )</span>
                                    </span>
                                    <span v-if="detalleSeleccionado.gananciaParcial < 0">
                                        <span v-if="!montoOculto">( <i class="fa fa-arrow-down"></i> {{detalleSeleccionado.tipoMoneda}} *** )</span>
                                        <span v-else>( <i class="fa fa-arrow-down"></i> {{detalleSeleccionado.tipoMoneda}} {{detalleSeleccionado.gananciaParcial}} )</span>
                                    </span>
                                </h5>
                            </div>
                            <div>
                                <div class="d-flex">
                                    <h6 class="mb-0">Rendimiento total</h6>
                                    &nbsp;
                                    <h6 class="mb-0" :class="colorPorEstadoGanancia(detalleSeleccionado.gananciaTotal)">
                                        <p v-if="!montoOculto">*** {{detalleSeleccionado.tipoMoneda === 'ARS' ? '$' : detalleSeleccionado.tipoMoneda}}</p>
                                        <p v-else>{{detalleSeleccionado.gananciaTotal}} {{detalleSeleccionado.tipoMoneda === 'ARS' ? '$' : detalleSeleccionado.tipoMoneda}}</p>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col pt-3 pb-3 d-flex justify-content-center">
                        <canvas id="porcentajesActivos" class="chartjs" width="770" height="385" style="display: block; width: 770px; height: 385px;"></canvas>
                    </div>
                </div>
            </div>
    `
});