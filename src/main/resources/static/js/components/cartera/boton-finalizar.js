Vue.component("boton-finalizar", {
    props: ["idActivo"],
    methods: {
        rescateTotalInversion(idActivo) {
            this.$emit("rescateTotalInversion", idActivo);
        },
        rescateParcial(activoId, monto) {
            this.$emit("rescateParcial", activoId, monto);
        },
        verModal() {
            $("#" + this.idActivo).modal("show");
        }
    },
    template: `
       
        <div>
            <button class="btn btn-primary btn-block boton-acciones" @click="verModal">Retirar</button>
            <confirmacion-eliminar @rescateTotalInversion="rescateTotalInversion" @rescateParcial="rescateParcial" :idActivo="idActivo"/>
        </div>
    `

});