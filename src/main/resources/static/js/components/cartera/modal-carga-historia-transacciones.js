Vue.component("modal-carga-historia-transacciones", {
    props: ["idActivo"],
    data: function () {
        return {
            idModal: "",
            transacciones: [],
            botonQuitarTransaccionDeshabilitado: true
        };
    },
    methods: {
        finalizarCarga() {
            this.confirmarTransaccion();
            if (confirm("¿Estás seguro que queres finalizar la carga?")) {
                this.modalDismiss();
                this.$emit("refrescarCartera");
            }
        },
        modalDismiss() {
            $("#" + this.idModal).modal("hide");
        },
        cancelar() {
            this.modalDismiss();
            this.resetear();
        },
        confirmarTransaccion() {
            this.$emit("transaccionHistorica", this.idActivo, this.transacciones);
            this.resetear();
        },
        quitarTransaccion() {
            if (this.transacciones.length > 1) {
                this.transacciones.pop();
            }
        },
        agregarTransaccion() {
            const unaTransaccion = {
                montoInvertido: "",
                cotizacion: "",
                primeraTransaccion: false
            };
            this.transacciones.push(unaTransaccion);
        },
        resetear() {
            this.transacciones = [{
                montoInvertido: "",
                cotizacion: "",
                primeraTransaccion: true
            }];
            for(let i = 0 ; i < 8 ; i++) {
                this.transacciones.push({
                    montoInvertido: "",
                    cotizacion: ""
                });
            }
        }
    },
    watch: {
        transacciones() {
            if(this.transacciones.length > 1) {
                this.botonQuitarTransaccionDeshabilitado = null;
            } else {
                this.botonQuitarTransaccionDeshabilitado = true;
            }
        }
    },
    created() {
        this.resetear();
        this.idModal = this.idActivo + "carga-historial";
    },
    template: `
        <div class="modal fade" :id="idModal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" @keyup.esc="resetear">

                    <div class="modal-header">
                        <h5 class="modal-title">Obtener el valor medio</h5>
                    </div>
                    <form @submit="finalizarCarga" @submit.prevent>
                        <div class="modal-body">
                            <p>Ingresá desde la primera hasta la últma de las transacciones.</p>
                            
                            <div class="scroll-input-transacciones pl-0 pr-0">
                                <div data-offset="0" v-for="transaccion in transacciones" class="row mb-1 pl-0 pr-0">
                                    <div class="col">
                                        <input
                                            placeholder="Monto invertido"
                                            class="form-control form-control-sm" 
                                            v-model="transaccion.montoInvertido"
                                            required
                                            type="number" step=0.01 
                                            min="0" max="999999999999999"
                                        />
                                    </div>
                                    <strong>-</strong>
                                    <div class="col">
                                        <input 
                                            placeholder="Cotización"
                                            class="form-control form-control-sm" 
                                            v-model="transaccion.cotizacion"
                                            required
                                            type="number" step=0.01 
                                            min="0" max="999999999999999"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mt-3 col">
                                    <button type="button" class="btn btn-danger btn-sm col" @click="quitarTransaccion" :disabled="botonQuitarTransaccionDeshabilitado"><i class="fa fa-minus"></i></button>
                                </div>
                                <div class="mt-3 col">
                                    <button type="button" class="btn btn-success btn-sm col" @click="agregarTransaccion"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-outline-secondary mr-2" @click="cancelar">Cancelar</button>
                            <button type="submit" class="btn btn-sm btn-primary ml-2">Confirmar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   `
});

