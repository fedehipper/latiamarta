Vue.component("actualizacion-monto", {
    props: ["idActivo", "montoActual", "accion", "titulo", "valorInput", "tituloInput"],
    data: function () {
        return {
            idModal: "",
            montoActualizado: this.valorInput,
            montoRecibido: false
        };
    },
    methods: {
        confirmar() {
            this.$emit("actualizarMonto", this.idActivo, this.montoActualizado, this.montoRecibido);
            $("#" + this.idModal).modal("hide");
            this.montoRecibido = false;
        }
    },
    created() {
        this.idModal = this.idActivo + this.accion;
    },
    template: `
        <div class="modal fade" :id="idModal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{titulo}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form @submit="confirmar" @submit.prevent>
                        <div class="modal-body">
                            <div class="pb-1">{{tituloInput}}</div>
                            <div class="col p-0">
                                <div class="row-lg">
                                    <div class="input-group-lg mb-3">
                                        <input 
                                            type="number" step=0.01
                                            class="form-control input-actualizacion"
                                            required
                                            min="0" max="999999999999999"
                                            v-model="montoActualizado">
                                    </div>
                                </div>
                            </div>
                            <div v-if="accion === 'inversion'" class="mt-3">
                                    <label>
                                        ¿Este monto es recibido/regalo?
                                    </label>
                                    <input
                                        class="form-check-input fa-clickable ml-5"
                                        v-model="montoRecibido" 
                                        name="gridRadios"
                                        type="checkbox">
                                    <span class="ml-1" v-if="montoRecibido">Si</span>
                                    <span class="ml-1" v-else>No</span>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-outline-secondary mr-2" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-sm btn-primary ml-2">Aceptar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
`
});
