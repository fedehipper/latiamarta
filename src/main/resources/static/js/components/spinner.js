Vue.component("spinner", {
    template: `
        <div class="text-center posicion-spinner d-flex flex-column align-items-center justify-content-end">
            <div class="justify-content-between">
                <div class="pb-2 spinner-grow spinner text-success" role="status"></div>
                <div>
                    <span class="text-success">Cargando...</span>
                </div>
            </div>
        </div>
    `
});

