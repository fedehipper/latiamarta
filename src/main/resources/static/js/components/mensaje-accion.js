Vue.component("mensaje-accion", {
    props: ["mensaje", "color"],
    template: `
        <div aria-live="polite" aria-atomic="true" style="position: relative;">
            <div class="toast toast-custom" :class=color data-delay="6000" data-autohide="true">
                <div class="toast-body">
                    {{mensaje}}
                </div>
            </div>
        </div>
`
});