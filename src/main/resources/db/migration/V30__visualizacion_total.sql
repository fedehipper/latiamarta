CREATE TABLE visualizacion_total(
    id BIGSERIAL PRIMARY KEY,
    monto_oculto BOOLEAN,
    usuario_id BIGINT,
    FOREIGN KEY (usuario_id) REFERENCES usuario(id)
);