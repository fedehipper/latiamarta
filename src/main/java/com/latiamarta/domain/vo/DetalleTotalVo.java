package com.latiamarta.domain.vo;

import com.latiamarta.domain.TipoMoneda;

public class DetalleTotalVo {

    private TipoMoneda tipoMoneda;
    private String montoActual;
    private String gananciaParcial;
    private String gananciaTotal;

    public TipoMoneda getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getMontoActual() {
        return montoActual;
    }

    public void setMontoActual(String montoActual) {
        this.montoActual = montoActual;
    }

    public String getGananciaParcial() {
        return gananciaParcial;
    }

    public void setGananciaParcial(String gananciaParcial) {
        this.gananciaParcial = gananciaParcial;
    }

    public String getGananciaTotal() {
        return gananciaTotal;
    }

    public void setGananciaTotal(String gananciaTotal) {
        this.gananciaTotal = gananciaTotal;
    }

}
