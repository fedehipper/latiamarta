package com.latiamarta.domain.vo;

import java.math.BigDecimal;

public class CotizacionDolarVo {

    private BigDecimal venta;

    public BigDecimal getVenta() {
        return venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

}
