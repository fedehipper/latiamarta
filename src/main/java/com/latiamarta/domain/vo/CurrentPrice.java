package com.latiamarta.domain.vo;

public class CurrentPrice {
    
    private String usd;

    public String getUsd() {
        return usd;
    }

    public void setUsd(String usd) {
        this.usd = usd;
    }
    
}
