package com.latiamarta.domain.vo;

import com.fasterxml.jackson.annotation.JsonGetter;

public class CotizacionCriptoMonedaVo {
    
    private MarketData marketData;
    private Image image;

    @JsonGetter("market_data")
    public MarketData getMarketData() {
        return marketData;
    }

    public void setMarketPrice(MarketData marketData) {
        this.marketData = marketData;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    
}
