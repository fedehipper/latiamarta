package com.latiamarta.domain.vo;

import com.latiamarta.domain.TipoMoneda;

public class DetalleActivoVo {

    private String nombre;
    private String montoActual;
    private String gananciaParcial;
    private String gananciaTotal;
    private String valorMedio;
    private boolean criptoMoneda;
    private TipoMoneda moneda;
    private String criptomonedaImagenUrl;
    private Long id;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCriptomonedaImagenUrl() {
        return criptomonedaImagenUrl;
    }

    public void setCriptomonedaImagenUrl(String criptomonedaImagenUrl) {
        this.criptomonedaImagenUrl = criptomonedaImagenUrl;
    }

    public boolean isCriptoMoneda() {
        return criptoMoneda;
    }

    public void setCriptoMoneda(boolean criptoMoneda) {
        this.criptoMoneda = criptoMoneda;
    }

    public String getMontoActual() {
        return montoActual;
    }

    public void setMontoActual(String montoActual) {
        this.montoActual = montoActual;
    }

    public String getValorMedio() {
        return valorMedio;
    }

    public void setValorMedio(String valorMedio) {
        this.valorMedio = valorMedio;
    }

    public String getGananciaParcial() {
        return gananciaParcial;
    }

    public void setGananciaParcial(String gananciaParcial) {
        this.gananciaParcial = gananciaParcial;
    }

    public String getGananciaTotal() {
        return gananciaTotal;
    }

    public void setGananciaTotal(String gananciaTotal) {
        this.gananciaTotal = gananciaTotal;
    }

    public TipoMoneda getMoneda() {
        return moneda;
    }

    public void setMoneda(TipoMoneda moneda) {
        this.moneda = moneda;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
