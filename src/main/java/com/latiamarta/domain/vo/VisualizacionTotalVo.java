package com.latiamarta.domain.vo;

public class VisualizacionTotalVo {

    private boolean montoOculto;

    public boolean isMontoOculto() {
        return montoOculto;
    }

    public void setMontoOculto(boolean montoOculto) {
        this.montoOculto = montoOculto;
    }
    
}
