package com.latiamarta.domain.vo;

import com.fasterxml.jackson.annotation.JsonGetter;

public class MarketData {
    
    private CurrentPrice currentPrice;

    @JsonGetter("current_price")
    public CurrentPrice getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(CurrentPrice currentPrice) {
        this.currentPrice = currentPrice;
    }
       
}
