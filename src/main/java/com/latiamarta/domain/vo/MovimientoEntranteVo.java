package com.latiamarta.domain.vo;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MovimientoEntranteVo {

    private LocalDateTime fecha;
    private BigDecimal monto;
    private String nombre;
    private Long id;
    private String moneda;
    private String tipo;
    private String criptomonedaId;
    private boolean criptomonedaPrimeraInversion;
    private String criptomonedaImagenUrl;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCriptomonedaImagenUrl() {
        return criptomonedaImagenUrl;
    }

    public void setCriptomonedaImagenUrl(String criptomonedaImagenUrl) {
        this.criptomonedaImagenUrl = criptomonedaImagenUrl;
    }
    
    public boolean isCriptomonedaPrimeraInversion() {
        return criptomonedaPrimeraInversion;
    }

    public void setCriptomonedaPrimeraInversion(boolean criptomonedaPrimeraInversion) {
        this.criptomonedaPrimeraInversion = criptomonedaPrimeraInversion;
    }

    public String getCriptomonedaId() {
        return criptomonedaId;
    }

    public void setCriptomonedaId(String criptomonedaId) {
        this.criptomonedaId = criptomonedaId;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal montoActual) {
        this.monto = montoActual;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

}
