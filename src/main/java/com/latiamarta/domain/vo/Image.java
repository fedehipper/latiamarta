package com.latiamarta.domain.vo;

public class Image {

    private String large;

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
    
}
