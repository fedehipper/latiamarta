package com.latiamarta.domain.vo;

import java.math.BigDecimal;

public class TransaccionHistoricaVo {
    
    private BigDecimal montoInvertido;
    private BigDecimal cotizacion;
    private boolean primeraTransaccion;
    
    public BigDecimal getMontoInvertido() {
        return montoInvertido;
    }

    public void setMontoInvertido(BigDecimal montoInvertido) {
        this.montoInvertido = montoInvertido;
    }

    public BigDecimal getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(BigDecimal cotizacion) {
        this.cotizacion = cotizacion;
    }

    public boolean isPrimeraTransaccion() {
        return primeraTransaccion;
    }

    public void setPrimeraTransaccion(boolean primerTransaccion) {
        this.primeraTransaccion = primerTransaccion;
    }

}
