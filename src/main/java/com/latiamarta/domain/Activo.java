package com.latiamarta.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Activo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    @Enumerated(EnumType.STRING)
    private TipoMoneda tipoMoneda;
    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
    private String criptomonedaId;
    private boolean criptomonedaPrimeraInversion;
    private String criptomonedaImagenUrl;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getCriptomonedaImagenUrl() {
        return criptomonedaImagenUrl;
    }

    public void setCriptomonedaImagenUrl(String criptomonedaImagenUrl) {
        this.criptomonedaImagenUrl = criptomonedaImagenUrl;
    }

    public boolean isCriptomonedaPrimeraInversion() {
        return criptomonedaPrimeraInversion;
    }

    public void setCriptomonedaPrimeraInversion(boolean criptomonedaPrimeraInversion) {
        this.criptomonedaPrimeraInversion = criptomonedaPrimeraInversion;
    }

    public String getCriptomonedaId() {
        return criptomonedaId;
    }

    public void setCriptomonedaId(String criptomonedaId) {
        this.criptomonedaId = criptomonedaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoMoneda getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

}
