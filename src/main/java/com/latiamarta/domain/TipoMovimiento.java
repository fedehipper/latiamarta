package com.latiamarta.domain;

public enum TipoMovimiento {
    INVERSION, ACTUALIZACION, RESCATE
}
