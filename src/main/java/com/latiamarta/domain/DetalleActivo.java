package com.latiamarta.domain;

import java.math.BigDecimal;

public class DetalleActivo {

    private String nombre;
    private TipoMoneda moneda;
    private BigDecimal montoActual;
    private BigDecimal gananciaParcial;
    private BigDecimal gananciaTotal;
    private BigDecimal valorMedio;
    private boolean criptomoneda;
    private String criptomonedaImagenUrl;
    private Long id;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCriptomonedaImagenUrl() {
        return criptomonedaImagenUrl;
    }

    public void setCriptomonedaImagenUrl(String criptomonedaImagenUrl) {
        this.criptomonedaImagenUrl = criptomonedaImagenUrl;
    }

    public boolean isCriptomoneda() {
        return criptomoneda;
    }

    public void setCriptomoneda(boolean criptomoneda) {
        this.criptomoneda = criptomoneda;
    }
    
    public BigDecimal getValorMedio() {
        return valorMedio;
    }

    public void setValorMedio(BigDecimal valorMedio) {
        this.valorMedio = valorMedio;
    }

    public TipoMoneda getMoneda() {
        return moneda;
    }

    public void setMoneda(TipoMoneda moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoActual() {
        return montoActual;
    }

    public void setMontoActual(BigDecimal montoActual) {
        this.montoActual = montoActual;
    }

    public BigDecimal getGananciaParcial() {
        return gananciaParcial;
    }

    public void setGananciaParcial(BigDecimal gananciaParcial) {
        this.gananciaParcial = gananciaParcial;
    }

    public BigDecimal getGananciaTotal() {
        return gananciaTotal;
    }

    public void setGananciaTotal(BigDecimal gananciaTotal) {
        this.gananciaTotal = gananciaTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
