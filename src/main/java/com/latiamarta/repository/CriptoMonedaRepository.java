package com.latiamarta.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CriptoMonedaVo;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Repository
public class CriptoMonedaRepository {
   
    private final RestTemplate restTemplate;

    @Value("${com.latiamarta.lista.criptomonedas.url}")
    private String urlListaCriptoMonedas;
    
    @Value("${com.latiamarta.cotizacion.criptomoneda.url}")
    private String urlCotizacionCriptomoneda;
    
    public CriptoMonedaRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    public List<CriptoMonedaVo> buscarTodas() throws JsonProcessingException {
        CriptoMonedaVo[] criptoMonedasObtenidas = restTemplate.getForObject(urlListaCriptoMonedas, CriptoMonedaVo[].class);

        return Arrays.asList(criptoMonedasObtenidas);
    }

    public CotizacionCriptoMonedaVo buscarCotizacionPorIdCriptomoneda(String criptomonedaId) {
        try {
            CotizacionCriptoMonedaVo cotizacionCriptomonedaVo = restTemplate.getForObject(urlCotizacionCriptomoneda + "/" + criptomonedaId, CotizacionCriptoMonedaVo.class);
            return cotizacionCriptomonedaVo;
        } catch (RestClientException exception) {
            throw new NoSuchElementException();
        }
    }
    
    
}
