package com.latiamarta.repository;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.TipoMoneda;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivoRepository extends JpaRepository<Activo, Long> {

    List<Activo> findByUsuarioUsername(String nombreUsuario);
    
    List<Activo> findByUsuarioUsernameAndTipoMoneda(String nombreUsuario, TipoMoneda tipoMoneda);

    Activo findByIdAndUsuarioUsername(Long id, String nombreUsuario);
}
