package com.latiamarta.repository;

import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.VisualizacionTotal;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisualizacionTotalRepository extends JpaRepository<VisualizacionTotal, Long> {
        
    Optional<VisualizacionTotal> findByUsuario(Usuario usuario);
    
}
