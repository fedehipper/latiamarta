package com.latiamarta.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.latiamarta.domain.vo.CotizacionDolarVo;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class CotizacionDolarRepository {

    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;

    @Value("${com.latiamarta.cotizacion.dolar.url}")
    private String urlCotizacion;

    public CotizacionDolarRepository(ObjectMapper objectMapper, RestTemplate restTemplate) {
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
    }

    public CotizacionDolarVo buscarCotizacionDolar() throws IOException {
        return objectMapper
                .readValue(restTemplate.getForObject(urlCotizacion, String.class), CotizacionDolarVo.class);
    }

}
