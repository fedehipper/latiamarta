package com.latiamarta.controller;

import com.latiamarta.domain.Usuario;
import com.latiamarta.service.CotizacionDolarService;
import com.latiamarta.service.UsuarioService;
import java.io.IOException;
import java.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CarteraController {

    private final CotizacionDolarService cotizacionDolarService;
    private final UsuarioService usuarioService;

    public CarteraController(CotizacionDolarService cotizacionDolarService, UsuarioService usuarioService) {
        this.cotizacionDolarService = cotizacionDolarService;
        this.usuarioService = usuarioService;
    }

    @GetMapping(value = {"/cartera", "/"})
    public ModelAndView cartera(ModelAndView modelAndView) throws IOException {
        int anioActual = LocalDate.now().getYear();
        modelAndView.addObject("anioActual", anioActual);

        modelAndView.addObject("cotizacionDolar", cotizacionDolarService.buscarCotizacion().getVenta());
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        modelAndView.addObject("nombreCompleto", usuario.getNombreCompleto());
        modelAndView.setViewName("cartera");
        return modelAndView;
    }

}
