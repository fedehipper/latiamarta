package com.latiamarta.controller.rest;

import com.latiamarta.domain.vo.DetalleTotalVo;
import com.latiamarta.service.DetalleActivoService;
import com.latiamarta.service.DetalleService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class DetalleMovimientoRestController {

    private final DetalleActivoService detalleMovimientoService;
    private final DetalleService detalleService;

    public DetalleMovimientoRestController(DetalleActivoService detalleMovimientoService,
            DetalleService detalleVoService) {
        this.detalleMovimientoService = detalleMovimientoService;
        this.detalleService = detalleVoService;
    }

    @GetMapping("detalle-activo")
    public List<DetalleTotalVo> obtenerDetalleMovimiento() {
        return detalleService
                .formatearDetalleTotal(detalleMovimientoService.calcularDetalleMovimientoTotal());
    }


}
