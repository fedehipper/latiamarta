package com.latiamarta.controller.rest;

import com.latiamarta.domain.VisualizacionTotal;
import com.latiamarta.domain.vo.VisualizacionTotalVo;
import com.latiamarta.service.VisualizacionTotalService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisualizacionTotalRestController {
    
    private final VisualizacionTotalService visualizacionTotalService;
    
    public VisualizacionTotalRestController(VisualizacionTotalService visualizacionTotalService) {
        this.visualizacionTotalService = visualizacionTotalService;
    }
    
    @GetMapping("/api/visualizacion-total")
    public VisualizacionTotalVo buscarVisualizacionTotal() {
        return visualizacionTransformerVo(visualizacionTotalService.buscarVisualizacionTotal());
    }
    
    @PutMapping("/api/visualizacion-total")
    public VisualizacionTotalVo modificarVisualizacionTotal() {
        return visualizacionTransformerVo(visualizacionTotalService.modificarVisualizacionTotal());
    }
    
    private VisualizacionTotalVo visualizacionTransformerVo(VisualizacionTotal visualizacionTotal) {
        VisualizacionTotalVo visualizacionTotalVo = new VisualizacionTotalVo();
        visualizacionTotalVo.setMontoOculto(visualizacionTotal.isMontoOculto());
        return visualizacionTotalVo;
    }
    
}
