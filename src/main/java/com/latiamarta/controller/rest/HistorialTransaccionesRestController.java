package com.latiamarta.controller.rest;

import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.vo.TransaccionHistoricaVo;
import com.latiamarta.service.HistorialTransaccionesService;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HistorialTransaccionesRestController {
    
    private final HistorialTransaccionesService historialTransaccionesService;
    
    public HistorialTransaccionesRestController(HistorialTransaccionesService historialTransaccionesService) {
        this.historialTransaccionesService = historialTransaccionesService;
    }
    
    @PostMapping("/api/transaccion-historica/{activoId}")
    public List<Movimiento> reiniciarHistorialTransacciones(@PathVariable Long activoId, @RequestBody List<TransaccionHistoricaVo> transaccionHistoricaVo) {
        return historialTransaccionesService.reemplazarHistorial(activoId, transaccionHistoricaVo);
    }
    
}
