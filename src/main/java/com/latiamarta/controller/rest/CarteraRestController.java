package com.latiamarta.controller.rest;

import com.latiamarta.domain.Porcentaje;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.domain.vo.DetalleActivoVo;
import com.latiamarta.service.CarteraService;
import com.latiamarta.service.DetalleService;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class CarteraRestController {

    private final CarteraService carteraService;
    private final DetalleService detalleVoService;
    
    public CarteraRestController(CarteraService carteraService, DetalleService detalleVoService) {
        this.carteraService = carteraService;
        this.detalleVoService = detalleVoService;
    }

    @GetMapping("cartera")
    public List<DetalleActivoVo> obtenerCartera() {
        return carteraService.obtenerCartera()
                .stream()
                .map(detalleActivo -> detalleVoService.formatearDetalleActivo(detalleActivo))
                .collect(toList());
    }

    @GetMapping("porcentaje")
    public List<Porcentaje> obtenerPorcentajes() throws IOException {
        return carteraService.obtenerPorcentaje();
    }

    @GetMapping("tipo-moneda")
    public Set<TipoMoneda> obtenerTipoMonedas() {
        return carteraService.obtenerTipoMonedasDisponiblesEnCartera();
    }
    
    @GetMapping("tipo-moneda-nueva-inversion")
    public List<TipoMoneda> obtenerTipoMonedasNuevaInversion() {
        return carteraService.obtenerTipoMonedasNuevaInversion();
    }
    
}
