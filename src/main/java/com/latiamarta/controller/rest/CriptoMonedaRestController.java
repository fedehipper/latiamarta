package com.latiamarta.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.latiamarta.domain.vo.CriptoMonedaVo;
import com.latiamarta.service.CriptoMonedaService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CriptoMonedaRestController {
    
    private final CriptoMonedaService criptomonedaService;
    
    public CriptoMonedaRestController(CriptoMonedaService criptomonedaService) {
        this.criptomonedaService = criptomonedaService;
    }
    
    @GetMapping("/api/criptomoneda/{simbolo}")
    public List<CriptoMonedaVo> buscarPorSimbolo(@PathVariable String simbolo) throws JsonProcessingException {
        return criptomonedaService.buscarCriptoMonedaPorSimbolo(simbolo);
    }
    
}
