package com.latiamarta.service;

import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.VisualizacionTotal;
import com.latiamarta.repository.VisualizacionTotalRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class VisualizacionTotalService {

    private final VisualizacionTotalRepository visualizacionTotalRepository;
    private final UsuarioService usuarioService;

    public VisualizacionTotalService(VisualizacionTotalRepository visualizacionTotalRepository, UsuarioService usuarioService) {
        this.visualizacionTotalRepository = visualizacionTotalRepository;
        this.usuarioService = usuarioService;
    }

    public VisualizacionTotal buscarVisualizacionTotal() {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        
        return visualizacionTotalRepository
            .findByUsuario(usuario)
            .orElseGet(() -> {
                VisualizacionTotal visualizacionTotal = new VisualizacionTotal();
                visualizacionTotal.setMontoOculto(true);
                visualizacionTotal.setUsuario(usuario);
                return visualizacionTotalRepository.save(visualizacionTotal);
            });
    }

    public VisualizacionTotal modificarVisualizacionTotal() {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        return visualizacionTotalRepository
            .findByUsuario(usuario)
            .map(visualizacionTotal -> {
                visualizacionTotal.setMontoOculto(!visualizacionTotal.isMontoOculto());
                return visualizacionTotal;
            })
            .get();
    }

}
