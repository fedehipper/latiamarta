package com.latiamarta.service;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.domain.Usuario;
import com.latiamarta.repository.ActivoRepository;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ActivoService {

    private final ActivoRepository activoRepository;
    private final UsuarioService usuarioService;

    public ActivoService(ActivoRepository activoRepository, UsuarioService usuarioService) {
        this.activoRepository = activoRepository;
        this.usuarioService = usuarioService;
    }

    public Activo guardar(String nombre, String tipoMoneda, String criptomonedaId, boolean isCriptomonedaPrimeraInversion, String criptomonedaImagenUrl) {
        return activoRepository
                .save(crearActivo(nombre, tipoMoneda, criptomonedaId, isCriptomonedaPrimeraInversion, criptomonedaImagenUrl));
    }

    public void eliminar(Long activoId) {
        Activo activoAEliminar = activoRepository
                .findById(activoId)
                .filter(activo -> usuarioService.getUsuarioLogueado().equals(activo.getUsuario().getUsername()))
                .orElseThrow(() -> new NoSuchElementException("Activo no encontrado."));
        activoRepository.delete(activoAEliminar);
    }

    public List<Activo> buscarTodos() {
        return activoRepository
                .findByUsuarioUsername(usuarioService.getUsuarioLogueado());
    }

    private Activo crearActivo(String nombre, String tipoMoneda, String criptomonedaId, boolean isCriptomonedaPrimeraInversion, String criptomonedaImagenUrl) {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Activo activo = new Activo();
        activo.setCriptomonedaId(criptomonedaId);
        activo.setTipoMoneda(TipoMoneda.valueOf(tipoMoneda));
        activo.setNombre(nombre);
        activo.setCriptomonedaPrimeraInversion(isCriptomonedaPrimeraInversion);
        activo.setUsuario(usuario);
        activo.setCriptomonedaImagenUrl(criptomonedaImagenUrl);
        return activo;
    }

}
