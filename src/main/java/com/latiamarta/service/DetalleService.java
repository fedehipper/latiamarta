package com.latiamarta.service;

import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.DetalleTotal;
import com.latiamarta.domain.vo.DetalleActivoVo;
import com.latiamarta.domain.vo.DetalleTotalVo;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.springframework.stereotype.Service;

@Service
public class DetalleService {

    public List<DetalleTotalVo> formatearDetalleTotal(List<DetalleTotal> detallesTotales) {
        return detallesTotales
                .stream()
                .map(this::transformarDetalleTotalADetalleTotalVo)
                .collect(toList());
    }

    public DetalleActivoVo formatearDetalleActivo(DetalleActivo detalleActivo) {
        DetalleActivoVo detalleActivoVo = new DetalleActivoVo();
        detalleActivoVo.setGananciaParcial(detalleActivo.getGananciaParcial().setScale(2).toString());
        detalleActivoVo.setGananciaTotal(detalleActivo.getGananciaTotal().setScale(2).toString());
        detalleActivoVo.setMontoActual(detalleActivo.getMontoActual().setScale(2).toString());
        detalleActivoVo.setNombre(detalleActivo.getNombre());
        detalleActivoVo.setCriptoMoneda(detalleActivo.isCriptomoneda());
        detalleActivoVo.setMoneda(detalleActivo.getMoneda());
        detalleActivoVo.setValorMedio(detalleActivo.getValorMedio() == null ? null : detalleActivo.getValorMedio().setScale(2).toString());
        detalleActivoVo.setId(detalleActivo.getId());
        detalleActivoVo.setCriptomonedaImagenUrl(detalleActivo.getCriptomonedaImagenUrl());
        return detalleActivoVo;
    }

    private DetalleTotalVo transformarDetalleTotalADetalleTotalVo(DetalleTotal detalleTotal) {
        DetalleTotalVo detalleTotalVo = new DetalleTotalVo();
        detalleTotalVo.setTipoMoneda(detalleTotal.getTipoMoneda());
        detalleTotalVo.setGananciaParcial(detalleTotal.getGananciaParcial().setScale(2).toString());
        detalleTotalVo.setGananciaTotal(detalleTotal.getGananciaTotal().setScale(2).toString());
        detalleTotalVo.setMontoActual(detalleTotal.getMontoActual().setScale(2).toString());
        return detalleTotalVo;
    }

}
