package com.latiamarta.service;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.TipoMovimiento;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.MovimientoEntranteVo;
import com.latiamarta.domain.vo.MovimientoSalienteVo;
import java.util.List;
import org.springframework.stereotype.Service;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.CriptoMonedaRepository;
import java.math.BigDecimal;
import static java.util.stream.Collectors.toList;
import com.latiamarta.repository.MovimientoRepository;
import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class MovimientoService {

    private final MovimientoRepository movimientoRepository;
    private final ActivoRepository activoRepository;
    private final ActivoService activoService;
    private final UsuarioService usuarioService;
    private final DetalleActivoService detalleActivoService;
    private final CriptoMonedaRepository criptoMonedaRepository;

    public MovimientoService(MovimientoRepository movimientoRepository,
            ActivoRepository activoRepository, ActivoService activoService,
            DetalleActivoService detalleActivoService,
            CriptoMonedaRepository criptoMonedaRepository,
            UsuarioService usuarioService) {
        this.movimientoRepository = movimientoRepository;
        this.detalleActivoService = detalleActivoService;
        this.activoRepository = activoRepository;
        this.activoService = activoService;
        this.usuarioService = usuarioService;
        this.criptoMonedaRepository = criptoMonedaRepository;
    }

    public List<MovimientoSalienteVo> buscarPorActivoId(Long activoId) {
        List<MovimientoSalienteVo> movimientos = movimientoRepository
                .findTop10ByUsuarioUsernameAndActivoIdOrderByFechaDesc(usuarioService.getUsuarioLogueado(), activoId)
                .stream()
                .map(this::convertirMovimientoAMovimientoSalienteVo)
                .collect(toList());
        if (movimientoRepository.countByActivoIdAndUsuarioUsername(activoId, usuarioService.getUsuarioLogueado()) <= 10) {
            movimientos.get(movimientos.size() - 1).setTipo("INVERSIÓN");
        }
        return movimientos;
    }

    public void eliminarMovimientoDeHoyPorActivoId(Long activoId) {
        String usuario = usuarioService.getUsuarioLogueado();

        if (movimientoRepository.findByActivoIdAndUsuarioUsername(activoId, usuario).size() == 1) {
            throw new IllegalArgumentException("Para deshacer un único movimiento debe rescatar todo el monto");
        }

        Movimiento ultimoMovimientoCreado = movimientoRepository
                .findTop10ByUsuarioUsernameAndActivoIdOrderByFechaDesc(usuario, activoId).get(0);

        movimientoRepository.deleteById(ultimoMovimientoCreado.getId());
    }

    public Movimiento realizarMovimientoTipoRescate(Long activoId, BigDecimal montoARescatar) {
        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoId);

        int comparacionMontos = detalleActivo.getMontoActual().subtract(montoARescatar).compareTo(BigDecimal.ZERO);

        switch (comparacionMontos) {
            case -1 -> throw new IllegalArgumentException("El monto a rescatar supera el disponible");
            case 0 -> throw new IllegalArgumentException("Seleccioná el rescate total.");
            default -> {
                Usuario usuario = usuarioService.buscarUsuarioLogueado();
                Activo activo = activoRepository.findByIdAndUsuarioUsername(activoId, usuario.getUsername());

                Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.now().withNano(0), montoARescatar, activo, RESCATE, usuario, false, false);

                return movimientoRepository.save(movimientoTipoRescate);
            }
        }
    }

    public Movimiento realizarMovimientoTipoInversion(Long activoId, BigDecimal montoAInvertir, boolean esRecibido) {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Activo activo = activoRepository.findByIdAndUsuarioUsername(activoId, usuario.getUsername());

        Movimiento movimientoTipoInversion = crearMovimiento(LocalDateTime.now().withNano(0), montoAInvertir, activo, INVERSION, usuario, false, esRecibido);

        return movimientoRepository.save(movimientoTipoInversion);
    }

    public void inicializarActivoYMovimiento(MovimientoEntranteVo movimientoVo) {
        Activo activo = activoService
            .guardar(movimientoVo.getNombre(), movimientoVo.getMoneda(), movimientoVo.getCriptomonedaId(), movimientoVo.isCriptomonedaPrimeraInversion(), movimientoVo.getCriptomonedaImagenUrl());
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Movimiento movimiento = crearMovimiento(LocalDateTime.now().withNano(0), movimientoVo.getMonto(), activo, ACTUALIZACION, usuario, true, false);

        movimientoRepository.save(movimiento);
    }

    public void eliminarMovimientosPorIdActivo(Long activoId) {
        movimientoRepository
                .findByUsuarioUsernameAndActivoIdOrderByFecha(usuarioService.getUsuarioLogueado(), activoId)
                .forEach(movimientoRepository::delete);
    }

    public Movimiento realizarMovimientoTipoActualizacion(MovimientoEntranteVo movimientoVo) {
        if (movimientoVo.getMonto() == null) {
            throw new IllegalArgumentException("No se ingreso monto nuevo");
        }
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Activo activo = activoRepository.findByIdAndUsuarioUsername(movimientoVo.getId(), usuario.getUsername());

        Movimiento movimiento = crearMovimiento(LocalDateTime.now().withNano(0), movimientoVo.getMonto(), activo, ACTUALIZACION, usuario, false, false);

        return movimientoRepository.save(movimiento);
    }

    private MovimientoSalienteVo convertirMovimientoAMovimientoSalienteVo(Movimiento movimiento) {
        MovimientoSalienteVo movimientoVo = new MovimientoSalienteVo();
        movimientoVo.setFecha(movimiento.getFecha().format(DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm")));
        movimientoVo.setMonto(movimiento.getMonto().setScale(2).toString());

        Map<TipoMovimiento, String> tipoMovimientoString = new HashMap<>();
        tipoMovimientoString.put(ACTUALIZACION, "ACTUALIZACIÓN");
        tipoMovimientoString.put(INVERSION, "INVERSIÓN");
        tipoMovimientoString.put(RESCATE, "RESCATE");

        movimientoVo.setTipo(tipoMovimientoString.get(movimiento.getTipo()));
        return movimientoVo;
    }

    private Movimiento crearMovimiento(LocalDateTime fecha, BigDecimal monto, Activo activo, TipoMovimiento tipo, Usuario usuario, boolean primerMovimiento, boolean esRecibido) {
        Movimiento movimiento = new Movimiento();
        movimiento.setFecha(fecha);
        movimiento.setMonto(monto);
        movimiento.setActivo(activo);
        if ((INVERSION.equals(tipo) || primerMovimiento) && activo.getCriptomonedaId() != null && activo.isCriptomonedaPrimeraInversion()) {
            CotizacionCriptoMonedaVo cotizacionCriptoMonedaVo = criptoMonedaRepository.buscarCotizacionPorIdCriptomoneda(activo.getCriptomonedaId());
            movimiento.setCotizacionActivo(new BigDecimal(cotizacionCriptoMonedaVo.getMarketData().getCurrentPrice().getUsd()));
        }
        movimiento.setMontoRecibido(esRecibido);
        movimiento.setTipo(tipo);
        movimiento.setUsuario(usuario);
        return movimiento;
    }

}
