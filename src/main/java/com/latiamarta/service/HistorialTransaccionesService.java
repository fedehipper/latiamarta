package com.latiamarta.service;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.TipoMovimiento;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.vo.TransaccionHistoricaVo;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.MovimientoRepository;
import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class HistorialTransaccionesService {

    private final ActivoRepository activoRepository;
    private final MovimientoRepository movimientoRepository;
    private final UsuarioService usuarioService;
    
    public HistorialTransaccionesService(ActivoRepository activoRepository, MovimientoRepository movimientoRepository, UsuarioService usuarioService) {
        this.activoRepository = activoRepository;
        this.movimientoRepository = movimientoRepository;
        this.usuarioService = usuarioService;
    }
    
    public List<Movimiento> reemplazarHistorial(Long activoId, List<TransaccionHistoricaVo> transaccionesHistoricas) {
        validarTransacciones(transaccionesHistoricas);
        Activo activo = activoRepository.findById(activoId).get();
        activo.setCriptomonedaPrimeraInversion(true);
        
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
                
        eliminarMovimientos(activoId, usuario.getUsername());
                
        return guardarMovimientoHistorico(activo, transaccionesHistoricas, usuario);
    }
    
    private void validarTransacciones(List<TransaccionHistoricaVo> transaccionesHistoricas) {
        transaccionesHistoricas.forEach(transaccion -> {
            if(transaccion.getCotizacion() == null || transaccion.getMontoInvertido() == null) {
                throw new IllegalArgumentException("Faltan valores de entrada.");
            }
        });
    }
    
    private void eliminarMovimientos(Long activoId, String username) {
        movimientoRepository
            .findByActivoIdAndUsuarioUsername(activoId, username)
            .stream()
            .map(Movimiento::getId)
            .forEach(movimientoRepository::deleteById);
    }
    
    private List<Movimiento> guardarMovimientoHistorico(Activo activo, List<TransaccionHistoricaVo> transaccionesHistoricas, Usuario usuario) {  
        return transaccionesHistoricas
            .stream()
            .map(transaccionHistorica -> {
                Movimiento movimientoHistorico = new Movimiento();
                movimientoHistorico.setActivo(activo);
                movimientoHistorico.setCotizacionActivo(transaccionHistorica.getCotizacion());
                movimientoHistorico.setFecha(LocalDateTime.now().withNano(0));
                movimientoHistorico.setMonto(transaccionHistorica.getMontoInvertido());
                if(transaccionHistorica.isPrimeraTransaccion()) {
                    movimientoHistorico.setTipo(TipoMovimiento.ACTUALIZACION);    
                } else {
                    movimientoHistorico.setTipo(TipoMovimiento.INVERSION);    
                }
                movimientoHistorico.setUsuario(usuario);
                return movimientoRepository.save(movimientoHistorico); 
            })
            .collect(toList());
    }
    
}
