package com.latiamarta.service;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.DetalleTotal;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.domain.TipoMovimiento;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import com.latiamarta.repository.ActivoRepository;
import java.math.BigDecimal;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.springframework.stereotype.Service;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import com.latiamarta.domain.Usuario;
import com.latiamarta.repository.MovimientoRepository;
import java.util.function.BinaryOperator;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import jakarta.transaction.Transactional;
import java.math.RoundingMode;
import java.util.Arrays;

@Service
@Transactional
public class DetalleActivoService {

    private final MovimientoRepository movimientoRepository;
    private final ActivoRepository activoRepository;
    private final UsuarioService usuarioService;

    public DetalleActivoService(
            ActivoRepository activoRepository,
            UsuarioService usuarioService,
            MovimientoRepository movimientoRepository) {
        this.movimientoRepository = movimientoRepository;
        this.activoRepository = activoRepository;
        this.usuarioService = usuarioService;
    }

    public List<DetalleTotal> calcularDetalleMovimientoTotal() {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        List<TipoMoneda> monedasEnInversion = buscarTipoMonedasEnInversion(usuario);

        return monedasEnInversion
                .stream()
                .map(monedaEnInversion -> {
                    List<Movimiento> movimientos = buscarMovimientosPorTipoMoneda(monedaEnInversion);

                    List<DetalleActivo> detallesActivos = detallesActivos(usuario.getUsername(), monedaEnInversion);
                    BigDecimal gananciaTotal = BigDecimal.ZERO;
                    BigDecimal gananciaParcial = gananciaParcial(detallesActivos);
                    BigDecimal montoActual = BigDecimal.ZERO;

                    for (DetalleActivo detalleActivo : detallesActivos) {
                        gananciaTotal = gananciaTotal.add(detalleActivo.getGananciaTotal());
                        montoActual = montoActual.add(detalleActivo.getMontoActual());
                    }
                    BigDecimal gananciaParcialResultante = (movimientos.size() == 1 ? BigDecimal.ZERO.setScale(2) : gananciaParcial);

                    return crearDetalleTotal(monedaEnInversion, gananciaParcialResultante, gananciaTotal, montoActual);
                })
                .collect(toList());
    }

    private BigDecimal gananciaParcial(List<DetalleActivo> detallesActivos) {
        return detallesActivos
                .stream()
                .map(detalleActivo -> detalleActivo.getGananciaParcial())
                .reduce(BigDecimal.ZERO, (unaGanancia, otraGanancia) -> unaGanancia.add(otraGanancia));
    }

    private List<DetalleActivo> detallesActivos(String username, TipoMoneda monedaEnInversion) {
        return activoRepository
                .findByUsuarioUsername(username)
                .stream()
                .filter(activo -> monedaEnInversion.equals(activo.getTipoMoneda()))
                .map(activoVigente -> calcularDetalleActivoIndividual(activoVigente.getId()))
                .collect(toList());
    }

    public DetalleActivo calcularDetalleActivoIndividual(Long activoId) {
        List<Movimiento> movimientos = movimientoRepository
                .findByUsuarioUsernameAndActivoIdOrderByFecha(usuarioService.getUsuarioLogueado(), activoId);
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Activo activo = activoRepository.findByIdAndUsuarioUsername(activoId, usuario.getUsername());

        return movimientos.isEmpty() ? null : calcularDetalleActivo(movimientos, activo);
    }

    private DetalleTotal crearDetalleTotal(TipoMoneda tipoMoneda,
            BigDecimal gananciaParcial, BigDecimal gananciaTotal,
            BigDecimal montoActual) {
        DetalleTotal detalleTotal = new DetalleTotal();
        detalleTotal.setTipoMoneda(tipoMoneda);
        detalleTotal.setGananciaParcial(gananciaParcial);
        detalleTotal.setGananciaTotal(gananciaTotal);
        detalleTotal.setMontoActual(montoActual);
        return detalleTotal;
    }

    private List<Movimiento> buscarMovimientosPorTipoMoneda(TipoMoneda tipoMoneda) {
        return movimientoRepository
                .findByUsuarioUsernameAndActivoTipoMoneda(usuarioService.getUsuarioLogueado(), tipoMoneda);
    }

    private final BinaryOperator<BigDecimal> sumatoria = (unMonto, otroMonto) -> unMonto.add(otroMonto);

    private DetalleActivo calcularDetalleActivo(List<Movimiento> movimientos, Activo activo) {
        Movimiento primerMovimiento = movimientos.get(0);
        List<Movimiento> movimientosDiarios = obtenerMovimientosDiarios(movimientos);
        Movimiento ultimoMovimientoDiario = ultimoMovimientoDiario(movimientosDiarios);
        DetalleActivo detalleActivo = new DetalleActivo();

        detalleActivo.setNombre(primerMovimiento.getActivo().getNombre());
        detalleActivo.setMoneda(primerMovimiento.getActivo().getTipoMoneda());
        detalleActivo.setGananciaParcial(determinarGananciaParcial(movimientos, movimientosDiarios));

        BigDecimal gananciaTotal = determinarGananciaTotal(movimientos);
        detalleActivo.setGananciaTotal(gananciaTotal);

        BigDecimal montoActual = ultimoMovimientoDiario
                .getMonto()
                .add(sumatoriaUltimosMovimientosSegunTipoDespuesDelUltimoDiario(movimientos, ultimoMovimientoDiario, INVERSION))
                .subtract(sumatoriaUltimosMovimientosSegunTipoDespuesDelUltimoDiario(movimientos, ultimoMovimientoDiario, RESCATE));

        detalleActivo.setValorMedio(calcularValorMedio(movimientos, activo));
        detalleActivo.setMontoActual(montoActual);
        detalleActivo.setId(primerMovimiento.getActivo().getId());
        detalleActivo.setCriptomoneda(activo.getCriptomonedaId() != null);
        detalleActivo.setCriptomonedaImagenUrl(activo.getCriptomonedaImagenUrl());
            
        return detalleActivo;
    }

    private BigDecimal calcularValorMedio(List<Movimiento> movimientos, Activo activo) {
        if (activo.getCriptomonedaId() != null && activo.isCriptomonedaPrimeraInversion()) {
            List<Movimiento> movimientosConCotizacion = movimientos
                    .stream()
                    .filter(movimiento -> movimiento.getCotizacionActivo() != null)
                    .collect(toList());

            BigDecimal sumaMonto = movimientosConCotizacion
                    .stream()
                    .map(movimiento -> movimiento.getMonto())
                    .reduce(BigDecimal.ZERO, sumatoria);

            return movimientosConCotizacion
                    .stream()
                    .map(movimiento -> movimiento.getCotizacionActivo().multiply(movimiento.getMonto()))
                    .reduce(BigDecimal.ZERO, sumatoria)
                    .divide(sumaMonto, 2, RoundingMode.FLOOR);
        } else {
            return null;
        }
    }

    private BigDecimal determinarGananciaTotal(List<Movimiento> movimientos) {
        BigDecimal gananciaTotal = BigDecimal.ZERO;
        BigDecimal montoAnterior = movimientos.get(0).getMonto();
        for (Movimiento movimiento : movimientos) {
            if (!Arrays.asList(INVERSION, RESCATE).contains(movimiento.getTipo())) {
                gananciaTotal = gananciaTotal.add(movimiento.getMonto().subtract(montoAnterior));
                montoAnterior = movimiento.getMonto();
            } else if (INVERSION.equals(movimiento.getTipo())) {
                if (!movimiento.isMontoRecibido()) {
                    montoAnterior = montoAnterior.add(movimiento.getMonto());
                }
            } else {
                montoAnterior = montoAnterior.subtract(movimiento.getMonto());
            }
        }
        return gananciaTotal;
    }

    private Movimiento ultimoMovimientoDiario(List<Movimiento> movimientosDiarios) {
        return movimientosDiarios.get(movimientosDiarios.size() - 1);
    }

    private BigDecimal determinarGananciaParcial(List<Movimiento> movimientos, List<Movimiento> movimientosDiarios) {
        if (movimientos.size() == 1 || movimientos.stream().filter(movimiento -> ACTUALIZACION.equals(movimiento.getTipo())).count() == 1) {
            return BigDecimal.ZERO;
        } else {
            Movimiento movimientoDiarioAnteriorAlUltimo = movimientosDiarios.get(movimientosDiarios.size() - 2);
            Movimiento movimientoDiarioMasReciente = movimientoDiarioMasReciente(movimientos);
            Movimiento movimientoAnteriorAlDiarioMasReciente = movimientoAnteriorAlDiarioMasReciente(movimientos, movimientoDiarioMasReciente);

            if (Arrays.asList(INVERSION, RESCATE).contains(movimientoAnteriorAlDiarioMasReciente.getTipo())) {
                return BigDecimal.ZERO;
            } else {
                return ultimoMovimientoDiario(movimientosDiarios)
                        .getMonto()
                        .subtract(movimientoDiarioAnteriorAlUltimo.getMonto());
            }
        }
    }

    private Movimiento movimientoDiarioMasReciente(List<Movimiento> movimientos) {
        Movimiento movimientoDiario = null;
        for (Movimiento movimiento : movimientos) {
            if (ACTUALIZACION.equals(movimiento.getTipo())) {
                movimientoDiario = movimiento;
            }
        }
        return movimientoDiario;
    }

    private Movimiento movimientoAnteriorAlDiarioMasReciente(List<Movimiento> movimientos, Movimiento movimientoDiarioMasReciente) {
        return movimientos.get(movimientos.indexOf(movimientoDiarioMasReciente) - 1);
    }

    private List<Movimiento> obtenerMovimientosDiarios(List<Movimiento> movimientos) {
        return movimientos
                .stream()
                .filter(movimiento -> ACTUALIZACION.equals(movimiento.getTipo()))
                .collect(toList());
    }

    private BigDecimal sumatoriaUltimosMovimientosSegunTipoDespuesDelUltimoDiario(List<Movimiento> movimientos, Movimiento movimientoMasReciente, TipoMovimiento tipoMovimiento) {
        return movimientos
                .stream()
                .filter(movimiento -> tipoMovimiento.equals(movimiento.getTipo()) && movimiento.getFecha().isAfter(movimientoMasReciente.getFecha()))
                .map(Movimiento::getMonto)
                .reduce(BigDecimal.ZERO, sumatoria);
    }

    private List<TipoMoneda> buscarTipoMonedasEnInversion(Usuario usuario) {
        return activoRepository
                .findByUsuarioUsername(usuario.getUsername())
                .stream()
                .map(Activo::getTipoMoneda)
                .distinct()
                .collect(toList());
    }

}
