package com.latiamarta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CriptoMonedaVo;
import com.latiamarta.repository.CriptoMonedaRepository;
import java.util.List;
import static java.util.stream.Collectors.toList;
import org.springframework.stereotype.Service;

@Service
public class CriptoMonedaService {
    
    private final CriptoMonedaRepository criptoMonedaRepository;
    
    public CriptoMonedaService(CriptoMonedaRepository criptoMonedaRepository) {
        this.criptoMonedaRepository = criptoMonedaRepository;
    }
   
    public List<CriptoMonedaVo> buscarCriptoMonedaPorSimbolo(String simboloCriptoMoneda) throws JsonProcessingException {
        return criptoMonedaRepository
                .buscarTodas()
                .stream()
                .filter(criptoMoneda -> simboloCriptoMoneda.equals(criptoMoneda.getSymbol()))
                .map(this::buscarImagen)
                .collect(toList());
    }
    
    private CriptoMonedaVo buscarImagen(CriptoMonedaVo criptoMonedaVo) {
        CotizacionCriptoMonedaVo cotizacionCriptoMonedaVo = criptoMonedaRepository
                .buscarCotizacionPorIdCriptomoneda(criptoMonedaVo.getId());
        criptoMonedaVo
                .setImagen(cotizacionCriptoMonedaVo.getImage().getLarge());
        return criptoMonedaVo;
    }
    
}
