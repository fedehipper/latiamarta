package com.latiamarta.service;

import com.latiamarta.domain.Usuario;
import com.latiamarta.repository.UsuarioRepository;
import jakarta.transaction.Transactional;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    public Usuario buscarUsuarioLogueado() {
        return usuarioRepository
                .findByUsername(getUsuarioLogueado())
                .get();
    }

    public String getUsuarioLogueado() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }

}
