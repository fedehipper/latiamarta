package com.latiamarta.service;

import com.latiamarta.domain.vo.CotizacionDolarVo;
import com.latiamarta.repository.CotizacionDolarRepository;
import java.io.IOException;
import org.springframework.stereotype.Service;

@Service
public class CotizacionDolarService {

    private final CotizacionDolarRepository cotizacionDolarRepository;

    public CotizacionDolarService(CotizacionDolarRepository cotizacionDolarRepository) {
        this.cotizacionDolarRepository = cotizacionDolarRepository;
    }

    public CotizacionDolarVo buscarCotizacion() throws IOException {
        return cotizacionDolarRepository.buscarCotizacionDolar();
    }

}
