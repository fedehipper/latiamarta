package com.latiamarta.service;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.Porcentaje;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.MovimientoRepository;
import jakarta.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CarteraService {

    private final MovimientoRepository movimientoRepository;
    private final DetalleActivoService detalleActivoService;
    private final ActivoRepository activoRepository;
    private final CotizacionDolarService cotizacionDolarService;
    private final UsuarioService usuarioService;

    public CarteraService(MovimientoRepository movimientoRepository,
            DetalleActivoService detalleActivoService,
            ActivoRepository activoRepository,
            CotizacionDolarService cotizacionDolarService,
            UsuarioService usuarioService) {
        this.movimientoRepository = movimientoRepository;
        this.detalleActivoService = detalleActivoService;
        this.activoRepository = activoRepository;
        this.cotizacionDolarService = cotizacionDolarService;
        this.usuarioService = usuarioService;
    }

    public List<DetalleActivo> obtenerCartera() {
        return buscarTodos()
                .stream()
                .map(movimiento -> movimiento.getActivo().getId())
                .distinct()
                .sorted()
                .map(detalleActivoService::calcularDetalleActivoIndividual)
                .collect(toList());
    }
    
    public List<TipoMoneda> obtenerTipoMonedasNuevaInversion() {
        return Arrays.asList(TipoMoneda.values());
    }

    public Set<TipoMoneda> obtenerTipoMonedasDisponiblesEnCartera() {
        return activoRepository
                .findByUsuarioUsername(usuarioService.getUsuarioLogueado())
                .stream()
                .map(Activo::getTipoMoneda)
                .collect(toSet());
    }

    public List<Porcentaje> obtenerPorcentaje() throws IOException {
        List<Activo> activos = activoRepository.findByUsuarioUsername(usuarioService.getUsuarioLogueado());

        if (activos.isEmpty()) {
            return new ArrayList<>();
        } else {
            BigDecimal cotizacionDolar = cotizacionDolarService.buscarCotizacion().getVenta().setScale(2);
            
            BigDecimal montoTotal = sumarMontos(activos, cotizacionDolar);
            return activos
                    .stream()
                    .map(activo -> {
                        Porcentaje porcentaje = new Porcentaje();
                        porcentaje.setNombreActivo(activo.getNombre());
                        porcentaje.setPorcentajeActivo(calculoPorcentaje(montoTotal, determinarMontoActualParaActivo(activo, cotizacionDolar)));
                        return porcentaje;
                    })
                    .collect(toList());
        }
    }

    private List<Movimiento> buscarTodos() {
        return movimientoRepository
                .findByUsuarioUsername(usuarioService.getUsuarioLogueado());
    }

    private BigDecimal determinarMontoActualParaActivo(Activo activo, BigDecimal cotizacionDolar) {
        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activo.getId());

        if (TipoMoneda.USD.equals(activo.getTipoMoneda())) {
            return detalleActivo.getMontoActual().multiply(cotizacionDolar).setScale(2, RoundingMode.FLOOR);
        } else {
            return detalleActivo.getMontoActual();
        }
    }

    private BigDecimal sumarMontos(List<Activo> activos, BigDecimal cotizacionDolar) {
        return activos
                .stream()
                .map(activo -> determinarMontoActualParaActivo(activo, cotizacionDolar))
                .reduce(BigDecimal.ZERO, (unMonto, otroMonto) -> unMonto.add(otroMonto));
    }

    private BigDecimal calculoPorcentaje(BigDecimal total, BigDecimal cantidad) {
        return cantidad
                .multiply(new BigDecimal("100.00"))
                .divide(total, 2, RoundingMode.FLOOR);
    }

}
