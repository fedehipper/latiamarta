package com.latiamarta;

import com.latiamarta.domain.Rol;
import com.latiamarta.domain.Usuario;
import com.latiamarta.repository.RolRepository;
import com.latiamarta.repository.UsuarioRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
@Transactional
public class ApplicationTests {

    @Autowired
    protected UsuarioRepository usuarioRepository;

    @Autowired
    protected RolRepository rolRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    protected Usuario usuario;
    protected Rol unRol;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setEnabled(true);
        usuario.setUsername("username");
        usuario.setPassword(passwordEncoder.encode("una contrasenia"));

        unRol = new Rol();
        unRol.setNombre("ROLE_USUARIO");
        rolRepository.save(unRol);

        usuarioRepository.save(usuario);
    }

    @Test
    public void contextLoads() {
    }

    protected Usuario guardarOtroUsuario() {
        Usuario otroUsuario = new Usuario();
        otroUsuario.setEnabled(true);
        otroUsuario.setUsername("otroUsuario");
        otroUsuario.setPassword("una contrasenia hasheada");

        Rol rolOtroUsuario = new Rol();
        rolOtroUsuario.setNombre("ROLE_USUARIO");
        rolRepository.save(rolOtroUsuario);

        return usuarioRepository.save(otroUsuario);
    }

}
