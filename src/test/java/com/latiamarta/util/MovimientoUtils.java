package com.latiamarta.util;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.TipoMovimiento;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.vo.MovimientoSalienteVo;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class MovimientoUtils {

    public static Movimiento crearMovimiento(LocalDateTime fecha, BigDecimal monto, Activo activo, TipoMovimiento tipo, Usuario usuario, boolean montoRecibido) {
        Movimiento movimiento = new Movimiento();
        movimiento.setFecha(fecha);
        movimiento.setMontoRecibido(montoRecibido);
        movimiento.setMonto(monto);
        movimiento.setActivo(activo);
        movimiento.setTipo(tipo);
        movimiento.setUsuario(usuario);
        return movimiento;
    }

    public static MovimientoSalienteVo convertirMovimientoAMovimientoSalienteVo(Movimiento movimiento) {
        MovimientoSalienteVo movimientoVo = new MovimientoSalienteVo();
        movimientoVo.setFecha(movimiento.getFecha().format(DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm")));
        movimientoVo.setMonto(movimiento.getMonto().setScale(2).toString());

        Map<TipoMovimiento, String> tipoMovimientoString = new HashMap<>();
        tipoMovimientoString.put(ACTUALIZACION, "ACTUALIZACIÓN");
        tipoMovimientoString.put(INVERSION, "INVERSIÓN");
        tipoMovimientoString.put(RESCATE, "RESCATE");

        movimientoVo.setTipo(tipoMovimientoString.get(movimiento.getTipo()));

        return movimientoVo;
    }

}
