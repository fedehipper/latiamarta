package com.latiamarta.util;

import com.latiamarta.domain.Activo;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.domain.Usuario;

public class ActivoUtils {

    public static Activo crearActivo(String nombre, TipoMoneda tipoMoneda, Usuario usuario, String criptomonedaId, boolean isCriptomonedaPrimeraInversion) {
        Activo activo = new Activo();
        activo.setNombre(nombre);
        activo.setTipoMoneda(tipoMoneda);
        activo.setCriptomonedaPrimeraInversion(isCriptomonedaPrimeraInversion);
        activo.setUsuario(usuario);
        activo.setCriptomonedaId(criptomonedaId);
        return activo;
    }

}
