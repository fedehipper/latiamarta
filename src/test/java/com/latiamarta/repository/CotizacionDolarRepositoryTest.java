package com.latiamarta.repository;

import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.vo.CotizacionDolarVo;
import java.io.IOException;
import java.math.BigDecimal;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class CotizacionDolarRepositoryTest extends ApplicationTests {

    @Autowired
    private CotizacionDolarRepository cotizacionDolarRepository;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @Value("${com.latiamarta.cotizacion.dolar.url}")
    private String cotizacionDolarUrl;

    @Test
    public void buscarCotizacionDolar_status200_seRecibeLaCotizacion() throws IOException {
        String uri = UriComponentsBuilder
                .fromHttpUrl(cotizacionDolarUrl)
                .toUriString();

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"venta\":\"10.00\"}", MediaType.TEXT_PLAIN));

        CotizacionDolarVo cotizacion = cotizacionDolarRepository.buscarCotizacionDolar();

        assertThat(cotizacion.getVenta()).isEqualTo(new BigDecimal("10.00"));

    }

}
