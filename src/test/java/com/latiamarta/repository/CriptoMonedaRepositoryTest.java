package com.latiamarta.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CriptoMonedaVo;
import com.latiamarta.domain.vo.CurrentPrice;
import com.latiamarta.domain.vo.Image;
import com.latiamarta.domain.vo.MarketData;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class CriptoMonedaRepositoryTest extends ApplicationTests {
    
    @Autowired
    private CriptoMonedaRepository criptomonedaRepository;
    
    @Autowired
    private MockRestServiceServer mockRestServiceServer;
    
    @Test
    public void buscarTodas_CriptomonedasExisten_retornaListaCriptomonedaVo() throws JsonProcessingException {
        
        CriptoMonedaVo btc = new CriptoMonedaVo();
        btc.setId("bitcoin");
        btc.setSymbol("btc");
        btc.setName("Bitcoin");
        
        CriptoMonedaVo ada = new CriptoMonedaVo();
        ada.setId("cardano");
        ada.setSymbol("ada");
        ada.setName("Cardano");
        
        List<CriptoMonedaVo> criptoMonedasEsperadas = Arrays.asList(btc, ada);
        
        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/list")
                .toUriString();

        ObjectMapper mapper = new ObjectMapper();
        String criptoMonedasEsperadasJson = mapper.writeValueAsString(criptoMonedasEsperadas);
        
        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(criptoMonedasEsperadasJson, MediaType.APPLICATION_JSON));

        List<CriptoMonedaVo> criptoMonedasObtenidas = criptomonedaRepository.buscarTodas();
        
        assertThat(criptoMonedasEsperadas.get(0)).isEqualToComparingFieldByField(criptoMonedasObtenidas.get(0));
        assertThat(criptoMonedasEsperadas.get(1)).isEqualToComparingFieldByField(criptoMonedasObtenidas.get(1));
    }
    
    @Test
    public void buscarCotizacionPorIdCriptomoneda_conIdExistente_retornaCotizacion() throws JsonProcessingException {
        String idBtc = "bitcoin";
        
        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/"+idBtc)
                .toUriString();
        
        CurrentPrice currentPrice = new CurrentPrice();
        currentPrice.setUsd("11.11");
        
        MarketData marketData = new MarketData();
        marketData.setCurrentPrice(currentPrice);
        
        Image image = new Image();
        image.setLarge("url-imagen-large");
        
        CotizacionCriptoMonedaVo cotizacionCriptomonedaVo = new CotizacionCriptoMonedaVo();
        cotizacionCriptomonedaVo.setMarketPrice(marketData);
        cotizacionCriptomonedaVo.setImage(image);
        
        ObjectMapper mapper = new ObjectMapper();
        String cotizacionEsperadaJson = mapper.writeValueAsString(cotizacionCriptomonedaVo);

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(cotizacionEsperadaJson, MediaType.APPLICATION_JSON));

       CotizacionCriptoMonedaVo cotizacion = criptomonedaRepository.buscarCotizacionPorIdCriptomoneda(idBtc);

       assertThat(cotizacionCriptomonedaVo.getMarketData().getCurrentPrice().getUsd())
               .isEqualTo(cotizacion.getMarketData().getCurrentPrice().getUsd());
       
       assertThat(cotizacionCriptomonedaVo.getImage().getLarge()).isEqualTo(cotizacion.getImage().getLarge());      
    }
    
    @Test
    public void buscarCotizacionPorIdCriptomoneda_conIdInexistente_lanzaExcepcion() throws JsonProcessingException {
        String idBtc = "idInexistente";
        
        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/"+idBtc)
                .toUriString();
  
        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));
       
        assertThatThrownBy(() -> criptomonedaRepository.buscarCotizacionPorIdCriptomoneda(idBtc))
                .isInstanceOf(NoSuchElementException.class);
        
    }
    
}
