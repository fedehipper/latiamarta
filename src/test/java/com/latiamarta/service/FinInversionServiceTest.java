package com.latiamarta.service;

import com.latiamarta.util.ActivoUtils;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.repository.ActivoRepository;
import java.math.BigDecimal;
import java.time.Month;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import com.latiamarta.repository.MovimientoRepository;
import static com.latiamarta.util.MovimientoUtils.crearMovimiento;
import org.springframework.security.test.context.support.WithMockUser;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

public class FinInversionServiceTest extends ApplicationTests {

    @Autowired
    private FinInversionService finInversionService;

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @WithMockUser("username")
    @Test
    public void finalizarInversion_activoValido_seEliminaActivosDiariosMasElActivoMaestro() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null ,false));
        Activo activoLete = activoRepository.save(ActivoUtils.crearActivo("LETE", null, usuario, null ,false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLete, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);

        finInversionService.finalizarInversion(activoLecaps.getId());

        assertThat(movimientoRepository.count()).isEqualTo(1);
        assertThat(activoRepository.count()).isEqualTo(1);
    }

}
