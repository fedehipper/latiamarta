package com.latiamarta.service;

import com.latiamarta.util.ActivoUtils;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.TipoMoneda;
import com.latiamarta.repository.ActivoRepository;
import java.util.List;
import java.util.NoSuchElementException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class ActivoServiceTest extends ApplicationTests {

    @Autowired
    private ActivoService activoService;

    @Autowired
    private ActivoRepository activoRepository;

    @WithMockUser("username")
    @Test
    public void guardar_nombreValido_seGuardaActivo() {
        String nombreActivo = "LECAPS";
        String tipoMoneda = "ARS";
        String criptomonedaId = "bitcoin";
        String criptomonedaImagenUrl = "url-criptomoneda";
        boolean isCriptomonedaPrimeraInversion = false;

        Activo activo = activoService.guardar(nombreActivo, tipoMoneda, criptomonedaId, isCriptomonedaPrimeraInversion, criptomonedaImagenUrl);

        assertThat(activo.getNombre()).isEqualTo(nombreActivo);
        assertThat(activo.getTipoMoneda()).isEqualTo(TipoMoneda.valueOf(tipoMoneda));
        assertThat(activo.getCriptomonedaId()).isEqualTo(criptomonedaId);
        assertThat(activo.getCriptomonedaImagenUrl()).isEqualTo(criptomonedaImagenUrl);
        assertThat(activoRepository.count()).isEqualTo(1);
    }

    @WithMockUser("username")
    @Test
    public void eliminar_activoExiste_seEliminaActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        activoService.eliminar(activoLecaps.getId());

        assertThat(activoRepository.count()).isEqualTo(0);
    }

    @WithMockUser("otroUsuario")
    @Test
    public void eliminar_activoExisteNoEsDeUsuarioLogueado_seEliminaActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        assertThatThrownBy(() -> activoService.eliminar(activoLecaps.getId()))
                .isInstanceOf(NoSuchElementException.class)
                .hasMessage("Activo no encontrado.");
    }

    @WithMockUser("username")
    @Test
    public void eliminar_activoNoExiste_lanzaExcepcion() {
        assertThatThrownBy(() -> activoService.eliminar(1L))
                .isInstanceOf(NoSuchElementException.class)
                .hasMessage("Activo no encontrado.");
    }

    @WithMockUser("otroUsuario")
    @Test
    public void buscarTodos_activosDeUnUsuario_retornaVacioParaOtroUsuario() {
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        List<Activo> activos = activoService.buscarTodos();

        assertTrue(activos.isEmpty());
    }

}
