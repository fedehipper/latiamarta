package com.latiamarta.service;

import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.DetalleTotal;
import com.latiamarta.domain.TipoMoneda;
import static com.latiamarta.domain.TipoMoneda.ARS;
import static com.latiamarta.domain.TipoMoneda.USD;
import com.latiamarta.domain.vo.DetalleActivoVo;
import com.latiamarta.domain.vo.DetalleTotalVo;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class DetalleServiceTest extends ApplicationTests {

    @Autowired
    private DetalleService detalleService;

   
    @WithMockUser("username")
    @Test
    public void detalleTotal_sinDecimales_detalleTotalVoConDecimales() {
       
        DetalleTotal unDetalleTotal = new DetalleTotal();
        unDetalleTotal.setTipoMoneda(ARS);
        unDetalleTotal.setGananciaParcial(new BigDecimal("4500"));
        unDetalleTotal.setGananciaTotal(new BigDecimal("8500"));
        unDetalleTotal.setMontoActual(new BigDecimal("30000.5"));

        DetalleTotal otroDetalleTotal = new DetalleTotal();
        otroDetalleTotal.setTipoMoneda(USD);
        otroDetalleTotal.setGananciaParcial(new BigDecimal("450"));
        otroDetalleTotal.setGananciaTotal(new BigDecimal("850"));
        otroDetalleTotal.setMontoActual(new BigDecimal("3000.5"));

        List<DetalleTotal> detalleTotal = Arrays.asList(unDetalleTotal, otroDetalleTotal);

        List<DetalleTotalVo> detalleTotalFormateado = detalleService.formatearDetalleTotal(detalleTotal);

        DetalleTotalVo detalleTotalFormateadoArs = detalleTotalFormateado
                .stream()
                .filter(detalle -> TipoMoneda.ARS.equals(detalle.getTipoMoneda()))
                .findFirst()
                .get();

        DetalleTotalVo detalleTotalFormateadoUsd = detalleTotalFormateado
                .stream()
                .filter(detalle -> TipoMoneda.USD.equals(detalle.getTipoMoneda()))
                .findFirst()
                .get();

        assertThat(detalleTotalFormateadoArs.getTipoMoneda()).isEqualTo(TipoMoneda.ARS);
        assertThat(detalleTotalFormateadoArs.getGananciaParcial()).isEqualTo("4500.00");
        assertThat(detalleTotalFormateadoArs.getGananciaTotal()).isEqualTo("8500.00");
        assertThat(detalleTotalFormateadoArs.getMontoActual()).isEqualTo("30000.50");

        assertThat(detalleTotalFormateadoUsd.getTipoMoneda()).isEqualTo(TipoMoneda.USD);
        assertThat(detalleTotalFormateadoUsd.getGananciaParcial()).isEqualTo("450.00");
        assertThat(detalleTotalFormateadoUsd.getGananciaTotal()).isEqualTo("850.00");
        assertThat(detalleTotalFormateadoUsd.getMontoActual()).isEqualTo("3000.50");
    }

    @Test
    public void detalleIndividual_sinDecimales_detalleTotalVoConDecimales() {
        String nombreActivo = "LECAPS";
        DetalleActivo detalleActivo = new DetalleActivo();
        detalleActivo.setGananciaParcial(new BigDecimal("4500"));
        detalleActivo.setGananciaTotal(new BigDecimal("8500"));
        detalleActivo.setMontoActual(new BigDecimal("30000.5"));
        detalleActivo.setNombre(nombreActivo);
        detalleActivo.setCriptomonedaImagenUrl("criptomoneda-imagen-url");
        detalleActivo.setCriptomoneda(false);
        detalleActivo.setValorMedio(new BigDecimal("7777"));
        detalleActivo.setMoneda(TipoMoneda.USD);
        detalleActivo.setId(1L);

        DetalleActivoVo detalleFormateado = detalleService.formatearDetalleActivo(detalleActivo);

        assertThat(detalleFormateado.getGananciaParcial()).isEqualTo("4500.00");
        assertThat(detalleFormateado.getGananciaTotal()).isEqualTo("8500.00");
        assertThat(detalleFormateado.getMontoActual()).isEqualTo("30000.50");
        assertThat(detalleFormateado.getValorMedio()).isEqualTo("7777.00");
        assertThat(detalleFormateado.getCriptomonedaImagenUrl()).isEqualTo(detalleActivo.getCriptomonedaImagenUrl());
        assertThat(detalleFormateado.getNombre()).isEqualTo(nombreActivo);
        assertThat(detalleFormateado.getMoneda()).isEqualTo(TipoMoneda.USD);
        assertThat(detalleFormateado.getId()).isEqualTo(1L);
        assertThat(detalleFormateado.isCriptoMoneda()).isEqualTo(false);
    }
    
    @Test
    public void detalleIndividual_sinCriptomonedaImagenUrl_detalleTotalVoConCriptomonedaImagenUrlNull() {
        String nombreActivo = "LECAPS";
        DetalleActivo detalleActivo = new DetalleActivo();
        detalleActivo.setGananciaParcial(new BigDecimal("4500"));
        detalleActivo.setGananciaTotal(new BigDecimal("8500"));
        detalleActivo.setMontoActual(new BigDecimal("30000.5"));
        detalleActivo.setNombre(nombreActivo);
        detalleActivo.setCriptomonedaImagenUrl(null);
        detalleActivo.setCriptomoneda(false);
        detalleActivo.setValorMedio(new BigDecimal("7777"));
        detalleActivo.setMoneda(TipoMoneda.USD);
        detalleActivo.setId(1L);

        DetalleActivoVo detalleFormateado = detalleService.formatearDetalleActivo(detalleActivo);

        assertThat(detalleFormateado.getCriptomonedaImagenUrl()).isEqualTo(detalleActivo.getCriptomonedaImagenUrl());
    }
    
    @Test
    public void detalleIndividual_conValorMedioNull_retornaDetalleConValorMedioFormateadoEnNull() {
        String nombreActivo = "LECAPS";
        DetalleActivo detalleActivo = new DetalleActivo();
        detalleActivo.setGananciaParcial(new BigDecimal("4500"));
        detalleActivo.setGananciaTotal(new BigDecimal("8500"));
        detalleActivo.setMontoActual(new BigDecimal("30000.5"));
        detalleActivo.setNombre(nombreActivo);
        detalleActivo.setCriptomoneda(true);
        detalleActivo.setValorMedio(null);
        detalleActivo.setMoneda(TipoMoneda.USD);
        detalleActivo.setId(1L);

        DetalleActivoVo detalleFormateado = detalleService.formatearDetalleActivo(detalleActivo);

        assertThat(detalleFormateado.getValorMedio()).isEqualTo(null);
        assertThat(detalleFormateado.isCriptoMoneda()).isEqualTo(true);
    }

}
