package com.latiamarta.service;

import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.vo.CotizacionDolarVo;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class CotizacionDolarServiceTest extends ApplicationTests {

    @Autowired
    private CotizacionDolarService cotizacionDolarService;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;
    
    @Value("${com.latiamarta.cotizacion.dolar.url}")
    private String cotizacionDolarUrl;

    @Test
    public void buscarCotizacion_conImpuestos_retornaCotizacionConImpuestos() throws IOException {

        String uri = UriComponentsBuilder.fromHttpUrl(cotizacionDolarUrl)
                .toUriString();

        BigDecimal cotizacionDolarSinImpuestos = new BigDecimal("80.97");
        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"venta\": " + cotizacionDolarSinImpuestos + " }", MediaType.TEXT_PLAIN));

        CotizacionDolarVo cotizacionDolar = cotizacionDolarService.buscarCotizacion();

        assertThat(cotizacionDolar.getVenta()).isEqualTo(cotizacionDolarSinImpuestos.setScale(2, RoundingMode.FLOOR));

    }

}
