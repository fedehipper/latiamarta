package com.latiamarta.service;

import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.VisualizacionTotal;
import com.latiamarta.repository.VisualizacionTotalRepository;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class VisualizacionTotalServiceTest extends ApplicationTests {

    @Autowired
    private VisualizacionTotalService visualizacionTotalService;

    @Autowired
    private VisualizacionTotalRepository visualizacionTotalRepository;

    @Test
    @WithMockUser("username")
    public void buscarVisualizacionTotal_conVisualizacionActivaYUsuarioLogueadoExiste_retornaVisualizacionActiva() {
        VisualizacionTotal visualizacionTotal = new VisualizacionTotal();
        visualizacionTotal.setMontoOculto(true);
        visualizacionTotal.setUsuario(usuario);
        visualizacionTotalRepository.save(visualizacionTotal);

        VisualizacionTotal visualizacionTotalEncontrada = visualizacionTotalService.buscarVisualizacionTotal();

        assertThat(visualizacionTotalEncontrada).isEqualTo(visualizacionTotal);
    }

    @Test
    @WithMockUser("username")
    public void buscarVisualizacionTotal_conVisualizacionActivaYUsuarioLogueadoNoExiste_seCreaEnActivaYRetornaVisualizacionActiva() {
        VisualizacionTotal visualizacionTotalEncontrada = visualizacionTotalService.buscarVisualizacionTotal();

        assertThat(visualizacionTotalEncontrada.getUsuario()).isEqualTo(usuario);
        assertThat(visualizacionTotalEncontrada.isMontoOculto()).isTrue();
        assertThat(visualizacionTotalEncontrada.getId()).isNotNull();
    }

    @Test
    @WithMockUser("username")
    public void buscarVisualizacionTotal_conVisualizacionActivaNoParaUsuarioLogueado_creaVisualizacionEnTrueYLaRetorna() {
        Usuario otroUsuario = guardarOtroUsuario();
        VisualizacionTotal visualizacionTotal = new VisualizacionTotal();
        visualizacionTotal.setMontoOculto(true);
        visualizacionTotal.setUsuario(otroUsuario);
        visualizacionTotalRepository.save(visualizacionTotal);

        VisualizacionTotal visualizacionTotalEncontrada = visualizacionTotalService.buscarVisualizacionTotal();

        assertThat(visualizacionTotalEncontrada.getUsuario()).isEqualTo(usuario);
        assertThat(visualizacionTotalEncontrada.isMontoOculto()).isTrue();
        assertThat(visualizacionTotalEncontrada.getId()).isNotNull();
    }
    
    @Test
    @WithMockUser("username")
    public void modificarVisualizacionTotal_conVisualizacionTotalTrue_cambiaAFalseAInversoYLoRetorna() {
        VisualizacionTotal visualizacionTotal = new VisualizacionTotal();
        visualizacionTotal.setMontoOculto(true);
        visualizacionTotal.setUsuario(usuario);
        visualizacionTotalRepository.save(visualizacionTotal);
        
        VisualizacionTotal visualizacionTotalCambiada = visualizacionTotalService.modificarVisualizacionTotal();
        
        assertThat(visualizacionTotalCambiada.getUsuario()).isEqualTo(usuario);
        assertThat(visualizacionTotalCambiada.isMontoOculto()).isFalse();
        assertThat(visualizacionTotalCambiada.getId()).isEqualTo(visualizacionTotal.getId());
    }
    
      
    @Test
    @WithMockUser("username")
    public void modificarVisualizacionTotal_conVisualizacionTotalFalse_cambiaATrueAInversoYLoRetorna() {
        VisualizacionTotal visualizacionTotal = new VisualizacionTotal();
        visualizacionTotal.setMontoOculto(false);
        visualizacionTotal.setUsuario(usuario);
        visualizacionTotalRepository.save(visualizacionTotal);
        
        VisualizacionTotal visualizacionTotalCambiada = visualizacionTotalService.modificarVisualizacionTotal();
        
        assertThat(visualizacionTotalCambiada.getUsuario()).isEqualTo(usuario);
        assertThat(visualizacionTotalCambiada.isMontoOculto()).isTrue();
        assertThat(visualizacionTotalCambiada.getId()).isEqualTo(visualizacionTotal.getId());
    }

}
