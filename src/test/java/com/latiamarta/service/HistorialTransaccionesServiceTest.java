package com.latiamarta.service;

import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.TipoMovimiento;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import com.latiamarta.domain.vo.TransaccionHistoricaVo;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.MovimientoRepository;
import com.latiamarta.util.ActivoUtils;
import static com.latiamarta.util.MovimientoUtils.crearMovimiento;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

public class HistorialTransaccionesServiceTest extends ApplicationTests {

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private HistorialTransaccionesService historialTransaccionesService;

    @WithMockUser("username")
    @Test
    public void reemplazarHistorial_activoCriptograficoSinHistorialInical_seReemplazaHistorialYSeGuardaMovimientoInicialTipoActualizacion() {
        Activo otro = activoRepository.save(ActivoUtils.crearActivo("Otro", null, usuario, "otro", false));
        Movimiento movimientoOtro = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), otro, ACTUALIZACION, usuario, false);
        movimientoRepository.save(movimientoOtro);

        Activo activo = activoRepository.save(ActivoUtils.crearActivo("BTC", null, usuario, "bitcoin", false));

        Movimiento movimientoInicial = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, INVERSION, usuario, false);
        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, RESCATE, usuario, false);

        movimientoRepository.save(movimientoInicial);
        movimientoRepository.save(movimientoInversion);
        movimientoRepository.save(movimientoActualizacion);
        movimientoRepository.save(movimientoRescate);

        BigDecimal cotizacion = new BigDecimal("10.0");
        BigDecimal montoInvertido = new BigDecimal("1.0");

        TransaccionHistoricaVo transaccionHistoricaVo = new TransaccionHistoricaVo();
        transaccionHistoricaVo.setCotizacion(cotizacion);
        transaccionHistoricaVo.setMontoInvertido(montoInvertido);
        transaccionHistoricaVo.setPrimeraTransaccion(true);

        List<TransaccionHistoricaVo> transacciones = new ArrayList<>();
        transacciones.add(transaccionHistoricaVo);

        historialTransaccionesService.reemplazarHistorial(activo.getId(), transacciones);

        Activo activoModificado = activoRepository.findById(activo.getId()).get();

        List<Movimiento> movimientos = movimientoRepository.findByActivoId(activo.getId());

        assertThat(activoModificado.isCriptomonedaPrimeraInversion()).isTrue();
        assertThat(movimientos.size()).isEqualTo(1);

        assertThat(movimientos.get(0).getMonto()).isEqualTo(transaccionHistoricaVo.getMontoInvertido());
        assertThat(movimientos.get(0).getCotizacionActivo()).isEqualTo(transaccionHistoricaVo.getCotizacion());
        assertThat(movimientos.get(0).getTipo()).isEqualTo(TipoMovimiento.ACTUALIZACION);
    }

    @WithMockUser("username")
    @Test
    public void reemplazarHistorial_activoCriptograficoConHistorialInicial_seReemplazaHistorialYSeGuardaMovimientoInicialTipoInversion() {
        Activo otro = activoRepository.save(ActivoUtils.crearActivo("Otro", null, usuario, "otro", false));
        Movimiento movimientoOtro = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), otro, ACTUALIZACION, usuario, false);
        movimientoRepository.save(movimientoOtro);

        Activo activo = activoRepository.save(ActivoUtils.crearActivo("BTC", null, usuario, "bitcoin", false));

        Movimiento movimientoInicial = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, INVERSION, usuario, false);
        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, RESCATE, usuario, false);

        movimientoRepository.save(movimientoInicial);
        movimientoRepository.save(movimientoInversion);
        movimientoRepository.save(movimientoActualizacion);
        movimientoRepository.save(movimientoRescate);

        BigDecimal cotizacion = new BigDecimal("10.0");
        BigDecimal montoInvertido = new BigDecimal("1.0");

        TransaccionHistoricaVo transaccionHistoricaVo = new TransaccionHistoricaVo();
        transaccionHistoricaVo.setCotizacion(cotizacion);
        transaccionHistoricaVo.setMontoInvertido(montoInvertido);
        transaccionHistoricaVo.setPrimeraTransaccion(true);

        TransaccionHistoricaVo otraTransaccionHistoricaVo = new TransaccionHistoricaVo();
        otraTransaccionHistoricaVo.setCotizacion(cotizacion);
        otraTransaccionHistoricaVo.setMontoInvertido(montoInvertido);
        otraTransaccionHistoricaVo.setPrimeraTransaccion(false);

        List<TransaccionHistoricaVo> transacciones = List.of(otraTransaccionHistoricaVo, transaccionHistoricaVo);

        historialTransaccionesService.reemplazarHistorial(activo.getId(), transacciones);

        Activo activoModificado = activoRepository.findById(activo.getId()).get();

        List<Movimiento> movimientos = movimientoRepository.findByActivoId(activo.getId());

        assertThat(activoModificado.isCriptomonedaPrimeraInversion()).isTrue();
        assertThat(movimientos.size()).isEqualTo(2);

        assertThat(movimientos.get(0).getMonto()).isEqualTo(transaccionHistoricaVo.getMontoInvertido());
        assertThat(movimientos.get(0).getCotizacionActivo()).isEqualTo(transaccionHistoricaVo.getCotizacion());
        assertThat(movimientos.get(0).getTipo()).isEqualTo(TipoMovimiento.INVERSION);

        assertThat(movimientos.get(1).getMonto()).isEqualTo(otraTransaccionHistoricaVo.getMontoInvertido());
        assertThat(movimientos.get(1).getCotizacionActivo()).isEqualTo(otraTransaccionHistoricaVo.getCotizacion());
        assertThat(movimientos.get(1).getTipo()).isEqualTo(TipoMovimiento.ACTUALIZACION);
    }

    @WithMockUser("username")
    @Test
    public void reemplazarHistorial_conCamposSinRellenarCotizacion_lanzaExcepcion() {
        Activo otro = activoRepository.save(ActivoUtils.crearActivo("Otro", null, usuario, "otro", false));
        Movimiento movimientoOtro = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), otro, ACTUALIZACION, usuario, false);
        movimientoRepository.save(movimientoOtro);

        Activo activo = activoRepository.save(ActivoUtils.crearActivo("BTC", null, usuario, "bitcoin", false));

        Movimiento movimientoInicial = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, INVERSION, usuario, false);
        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, RESCATE, usuario, false);

        movimientoRepository.save(movimientoInicial);
        movimientoRepository.save(movimientoInversion);
        movimientoRepository.save(movimientoActualizacion);
        movimientoRepository.save(movimientoRescate);

        BigDecimal montoInvertido = new BigDecimal("1.0");

        TransaccionHistoricaVo transaccionHistoricaVo = new TransaccionHistoricaVo();
        transaccionHistoricaVo.setCotizacion(null);
        transaccionHistoricaVo.setMontoInvertido(montoInvertido);
        transaccionHistoricaVo.setPrimeraTransaccion(true);

        List<TransaccionHistoricaVo> transacciones = new ArrayList<>();
        transacciones.add(transaccionHistoricaVo);

        assertThatThrownBy(() -> historialTransaccionesService.reemplazarHistorial(activo.getId(), transacciones))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Faltan valores de entrada.");
    }

    @WithMockUser("username")
    @Test
    public void reemplazarHistorial_conCamposSinRellenarMontoInvertido_lanzaExcepcion() {
        Activo otro = activoRepository.save(ActivoUtils.crearActivo("Otro", null, usuario, "otro", false));
        Movimiento movimientoOtro = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), otro, ACTUALIZACION, usuario, false);
        movimientoRepository.save(movimientoOtro);

        Activo activo = activoRepository.save(ActivoUtils.crearActivo("BTC", null, usuario, "bitcoin", false));

        Movimiento movimientoInicial = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, INVERSION, usuario, false);
        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, ACTUALIZACION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activo, RESCATE, usuario, false);

        movimientoRepository.save(movimientoInicial);
        movimientoRepository.save(movimientoInversion);
        movimientoRepository.save(movimientoActualizacion);
        movimientoRepository.save(movimientoRescate);

        BigDecimal cotizacion = new BigDecimal("1.0");

        TransaccionHistoricaVo transaccionHistoricaVo = new TransaccionHistoricaVo();
        transaccionHistoricaVo.setCotizacion(cotizacion);
        transaccionHistoricaVo.setMontoInvertido(null);
        transaccionHistoricaVo.setPrimeraTransaccion(true);

        List<TransaccionHistoricaVo> transacciones = new ArrayList<>();
        transacciones.add(transaccionHistoricaVo);

        assertThatThrownBy(() -> historialTransaccionesService.reemplazarHistorial(activo.getId(), transacciones))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Faltan valores de entrada.");

    }

}
