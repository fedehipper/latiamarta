package com.latiamarta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.latiamarta.util.ActivoUtils;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.Movimiento;
import static com.latiamarta.domain.TipoMoneda.ARS;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import com.latiamarta.domain.vo.MovimientoEntranteVo;
import com.latiamarta.repository.ActivoRepository;
import java.math.BigDecimal;
import java.time.Month;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import org.springframework.beans.factory.annotation.Autowired;
import com.latiamarta.repository.MovimientoRepository;
import static com.latiamarta.util.MovimientoUtils.crearMovimiento;
import java.util.List;
import org.springframework.security.test.context.support.WithMockUser;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CurrentPrice;
import com.latiamarta.domain.vo.MarketData;
import com.latiamarta.domain.vo.MovimientoSalienteVo;
import static com.latiamarta.util.MovimientoUtils.convertirMovimientoAMovimientoSalienteVo;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class MovimientoServiceTest extends ApplicationTests {

    @Autowired
    private MovimientoService movimientoService;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoActualizacion_nuevoMovimiento_seAgregaUnNuevoMovimiento() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));
        Activo activoLebacs = activoRepository.save(ActivoUtils.crearActivo("LEBACS", null, usuario, null, false));
        Activo activoMacro = activoRepository.save(ActivoUtils.crearActivo("MACRO", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoMacro, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLebacs, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setMonto(new BigDecimal("520.00"));
        movimientoVo.setId(activoLecaps.getId());

        Movimiento movimientoNuevo = movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        Activo activo = activoRepository.findById(activoLecaps.getId()).get();

        assertThat(movimientoNuevo.getMonto()).isEqualTo(movimientoVo.getMonto());
        assertThat(movimientoNuevo.getActivo().getNombre()).isEqualTo(activo.getNombre());
        assertThat(movimientoNuevo.getFecha()).isEqualToIgnoringSeconds(LocalDateTime.now());
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoActualizacion_nuevoMovimiento_guardaConFechaHoy() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setFecha(fecha);
        movimientoVo.setId(activoLecaps.getId());
        movimientoVo.setMonto(new BigDecimal("7777.77"));
        movimientoVo.setNombre(activoLecaps.getNombre());

        Movimiento movimientoGuardado = movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        assertThat(movimientoGuardado.getMonto()).isEqualTo(new BigDecimal("7777.77"));
        assertThat(movimientoGuardado.getActivo().getId()).isEqualTo(activoLecaps.getId());
        assertThat(movimientoGuardado.getTipo()).isEqualTo(ACTUALIZACION);
        assertThat(movimientoGuardado.getFecha()).isEqualToIgnoringSeconds(fecha);
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoActualizacion_montoNull_lanzaExcepcion() {
        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setFecha(LocalDateTime.now());

        assertThatThrownBy(() -> movimientoService.realizarMovimientoTipoActualizacion(movimientoVo))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @WithMockUser("otroUsuario")
    @Test
    public void realizarMovimientoTipoActualizacion_activoExisteEnBaseNoEsDeUsuarioLogueado_seCreaElActivoParaElUsuarioLogueado() {
        guardarOtroUsuario();
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setFecha(fecha);
        movimientoVo.setId(activoLecaps.getId());
        movimientoVo.setMonto(new BigDecimal("7777.77"));
        movimientoVo.setNombre("LECAPS");

        Movimiento movimientoNuevo = movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        assertThat(movimientoNuevo.getUsuario().getUsername()).isEqualTo("otroUsuario");
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoActualizacion_activoExisteEnBase_creaActivoYSeAsociaAlMovimiento() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setFecha(fecha);
        movimientoVo.setId(activoLecaps.getId());
        movimientoVo.setMonto(new BigDecimal("7777.77"));
        movimientoVo.setNombre("LECAPS");

        Movimiento movimientoNuevo = movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        assertThat(movimientoNuevo.getMonto()).isEqualTo(new BigDecimal("7777.77"));
        assertThat(movimientoNuevo.getActivo().getId()).isEqualTo(activoLecaps.getId());
        assertThat(movimientoNuevo.getFecha()).isEqualToIgnoringSeconds(fecha);
    }

    @WithMockUser("otroUsuario")
    @Test
    public void realizarMovimientoTipoActualizacion_ultimoMovimientoEsDeUsuarioDistintoAlLogueadoConMismaFechaQueElNuevo_guardadoExitoso() {
        guardarOtroUsuario();
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        LocalDateTime fecha = LocalDateTime.now().withSecond(0).withNano(0);

        Movimiento unMovimiento = crearMovimiento(fecha, new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setFecha(fecha);
        movimientoVo.setId(activoLecaps.getId());
        movimientoVo.setMonto(new BigDecimal("11000.00"));
        movimientoVo.setNombre("LECAPS");

        movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        assertThat(movimientoRepository.count()).isEqualTo(2);
    }

    @WithMockUser("username")
    @Test
    public void inicializarActivoYMovimiento_movimientoValido_guardaActivoYPrimerMovimiento() throws JsonProcessingException {
        mockCotizacionCriptomoneda();

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setMonto(new BigDecimal("1000.00"));
        movimientoVo.setMoneda("ARS");
        movimientoVo.setNombre("LECAPS");
        movimientoVo.setCriptomonedaImagenUrl("url-imagen");
        movimientoVo.setCriptomonedaId("bitcoin");
        movimientoVo.setCriptomonedaPrimeraInversion(true);

        movimientoService.inicializarActivoYMovimiento(movimientoVo);

        Activo activoCreado = activoRepository.findAll()
                .stream()
                .findFirst()
                .get();
        Movimiento movimientoInicial = movimientoRepository.findAll()
                .stream()
                .findFirst()
                .get();

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        assertThat(activoCreado.getNombre()).isEqualTo("LECAPS");
        assertThat(activoCreado.getTipoMoneda()).isEqualTo(ARS);
        assertThat(activoCreado.getCriptomonedaImagenUrl()).isEqualTo(movimientoVo.getCriptomonedaImagenUrl());
        assertThat(activoCreado.getCriptomonedaId()).isEqualTo(movimientoVo.getCriptomonedaId());
        assertThat(activoCreado.isCriptomonedaPrimeraInversion()).isEqualTo(movimientoVo.isCriptomonedaPrimeraInversion());

        assertThat(movimientoInicial.getFecha()).isEqualToIgnoringSeconds(fecha);
        assertThat(movimientoInicial.getMonto()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(movimientoInicial.getTipo()).isEqualTo(ACTUALIZACION);
        assertThat(movimientoInicial.getActivo().getId()).isEqualTo(activoCreado.getId());
        assertThat(movimientoInicial.getCotizacionActivo()).isEqualTo(new BigDecimal("11.11"));
    }

    @WithMockUser("username")
    @Test
    public void inicializarActivoYMovimiento_movimientoValidoSinCriptomonedaId_guardaActivoSinCotizacion() throws JsonProcessingException {
        mockCotizacionCriptomoneda();

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setMonto(new BigDecimal("1000.00"));
        movimientoVo.setMoneda("ARS");
        movimientoVo.setNombre("LECAPS");

        movimientoService.inicializarActivoYMovimiento(movimientoVo);

        Activo activoCreado = activoRepository.findAll()
                .stream()
                .findFirst()
                .get();
        Movimiento movimientoInicial = movimientoRepository.findAll()
                .stream()
                .findFirst()
                .get();

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        assertThat(activoCreado.getNombre()).isEqualTo("LECAPS");
        assertThat(activoCreado.getTipoMoneda()).isEqualTo(ARS);
        assertThat(activoCreado.getCriptomonedaId()).isEqualTo(movimientoVo.getCriptomonedaId());

        assertThat(movimientoInicial.getFecha()).isEqualToIgnoringSeconds(fecha);
        assertThat(movimientoInicial.getMonto()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(movimientoInicial.getTipo()).isEqualTo(ACTUALIZACION);
        assertThat(movimientoInicial.getActivo().getId()).isEqualTo(activoCreado.getId());
        assertThat(movimientoInicial.getCotizacionActivo()).isNull();
    }

    @WithMockUser("username")
    @Test
    public void inicializarActivoYMovimiento_conCriptomonedaIdYSinDatosHistoricosTransacciones_guardaActivoSinCotizacion() throws JsonProcessingException {
        mockCotizacionCriptomoneda();

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setMonto(new BigDecimal("1000.00"));
        movimientoVo.setMoneda("ARS");
        movimientoVo.setNombre("LECAPS");
        movimientoVo.setCriptomonedaId("bitcoin");
        movimientoVo.setCriptomonedaPrimeraInversion(false);

        movimientoService.inicializarActivoYMovimiento(movimientoVo);

        Activo activoCreado = activoRepository.findAll()
                .stream()
                .findFirst()
                .get();
        Movimiento movimientoInicial = movimientoRepository.findAll()
                .stream()
                .findFirst()
                .get();

        assertThat(activoCreado.isCriptomonedaPrimeraInversion()).isEqualTo(movimientoVo.isCriptomonedaPrimeraInversion());
        assertThat(movimientoInicial.getCotizacionActivo()).isNull();
    }

    private void mockCotizacionCriptomoneda() throws JsonProcessingException {
        String idBtc = "bitcoin";

        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/" + idBtc)
                .toUriString();

        CurrentPrice currentPrice = new CurrentPrice();
        currentPrice.setUsd("11.11");

        MarketData marketData = new MarketData();
        marketData.setCurrentPrice(currentPrice);

        CotizacionCriptoMonedaVo cotizacionCriptomonedaVo = new CotizacionCriptoMonedaVo();
        cotizacionCriptomonedaVo.setMarketPrice(marketData);

        ObjectMapper mapper = new ObjectMapper();
        String cotizacionEsperadaJson = mapper.writeValueAsString(cotizacionCriptomonedaVo);

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(cotizacionEsperadaJson, MediaType.APPLICATION_JSON));
    }

    @WithMockUser("username")
    @Test
    public void eliminarMovimientoDeHoyPorActivoId_movimientoExisteConFechaHoy_seEliminaElMovimiento() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        Movimiento unMovimiento = crearMovimiento(fecha.minusDays(1), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoEquivocado = crearMovimiento(fecha, new BigDecimal("500.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoEquivocado);

        movimientoService.eliminarMovimientoDeHoyPorActivoId(activoLecaps.getId());

        assertThat(movimientoRepository.findById(movimientoEquivocado.getId())).isEmpty();
    }

    @WithMockUser("username")
    @Test
    public void eliminarMovimientoDeHoyPorActivoId_movimientoExisteYEsUnicoPorActivoIdConFechaHoy_lanzaExcepcion() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento movimientoUnicoInicioInversion = crearMovimiento(LocalDateTime.now().withNano(0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(movimientoUnicoInicioInversion);

        assertThatThrownBy(() -> movimientoService.eliminarMovimientoDeHoyPorActivoId(activoLecaps.getId()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Para deshacer un único movimiento debe rescatar todo el monto");
    }

    @WithMockUser("username")
    @Test
    public void eliminarMovimientosPorIdActivo_movimientoExiste_seEliminaMovimiento() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento movimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, null, usuario, false);

        movimientoRepository.save(movimiento);

        movimientoService.eliminarMovimientosPorIdActivo(activoLecaps.getId());

        assertThat(movimientoRepository.count()).isEqualTo(0);
    }

    @WithMockUser("otroUsuario")
    @Test
    public void eliminarMovimientosPorIdActivo_movimientoExisteDeOtroUsuario_noSeEliminaMovimiento() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento movimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, null, usuario, false);

        movimientoRepository.save(movimiento);

        movimientoService.eliminarMovimientosPorIdActivo(activoLecaps.getId());

        assertThat(movimientoRepository.count()).isEqualTo(1);
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoInversion_paraActivoConCriptoMonedaId_seGuardaLaCotizacion() throws JsonProcessingException {
        mockCotizacionCriptomoneda();

        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, otroUsuario, "bitcoin", true));

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, "bitcoin", true));

        boolean montoRecibido = false;
        Movimiento movimientoTipoInversion = movimientoService
                .realizarMovimientoTipoInversion(activoLecaps.getId(), new BigDecimal("500.50"), montoRecibido);

        assertThat(movimientoTipoInversion.getCotizacionActivo()).isEqualTo(new BigDecimal("11.11"));
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoInversion_movimientoReferenciadoAEseActivoExiste_seSumaATodasLasFechasDelMovimientoLaNuevaInversion() {
        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, otroUsuario, null, true));

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        boolean montoRecibido = false;
        Movimiento movimientoTipoInversion = movimientoService
                .realizarMovimientoTipoInversion(activoLecaps.getId(), new BigDecimal("500.50"), montoRecibido);

        assertThat(movimientoTipoInversion.getMonto()).isEqualTo(new BigDecimal("500.50"));
        assertThat(movimientoTipoInversion.getFecha()).isEqualTo(LocalDateTime.now().withNano(0));
        assertThat(movimientoTipoInversion.getTipo()).isEqualTo(INVERSION);
        assertThat(movimientoTipoInversion.getActivo()).isEqualTo(activoLecaps);
        assertThat(movimientoTipoInversion.getCotizacionActivo()).isNull();
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoInversion_conMontoRecibido_seGuardaEnTrueMontoRecibido() {
        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, otroUsuario, null, true));

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        boolean montoRecibido = true;
        Movimiento movimientoTipoInversion = movimientoService
                .realizarMovimientoTipoInversion(activoLecaps.getId(), new BigDecimal("500.50"), montoRecibido);

        assertThat(movimientoTipoInversion.isMontoRecibido()).isTrue();
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoActualizacion_paraActivoConCriptoMonedaId_seAgregaUnNuevoMovimientoSinCotizacion() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, "bitcoin", true));
        Activo activoLebacs = activoRepository.save(ActivoUtils.crearActivo("LEBACS", null, usuario, "bitcoin", true));
        Activo activoMacro = activoRepository.save(ActivoUtils.crearActivo("MACRO", null, usuario, "bitcoin", true));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoMacro, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLebacs, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);

        MovimientoEntranteVo movimientoVo = new MovimientoEntranteVo();
        movimientoVo.setMonto(new BigDecimal("520.00"));
        movimientoVo.setId(activoLecaps.getId());

        Movimiento movimientoNuevo = movimientoService.realizarMovimientoTipoActualizacion(movimientoVo);

        assertThat(movimientoNuevo.getCotizacionActivo()).isNull();
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoRescate_paraActivoConCriptoMonedaId_seAgregaUnNuevoMovimientoSinCotizacion() {
        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, otroUsuario, "bitcoin", true));

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, "bitcoin", true));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5500.00"), activoLecaps, ACTUALIZACION, usuario, false);
        otroMovimiento.setCotizacionActivo(BigDecimal.TEN);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);

        Movimiento movimientoRescate = movimientoService
                .realizarMovimientoTipoRescate(activoLecaps.getId(), new BigDecimal("4000.00"));

        assertThat(movimientoRescate.getCotizacionActivo()).isNull();
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoRescate_montoMenorAlMontoActual_guardaElMontoEnMovimientoConTipoRescate() {
        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, otroUsuario, null, false));

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5500.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);

        Movimiento movimientoRescate = movimientoService
                .realizarMovimientoTipoRescate(activoLecaps.getId(), new BigDecimal("4000.00"));

        LocalDateTime fecha = LocalDateTime.now().withNano(0);

        assertThat(movimientoRescate.getTipo()).isEqualTo(RESCATE);
        assertThat(movimientoRescate.getFecha()).isEqualTo(fecha);
        assertThat(movimientoRescate.getMonto()).isEqualTo(new BigDecimal("4000.00"));
        assertThat(movimientoRescate.getCotizacionActivo()).isNull();
        assertThat(movimientoRescate.getActivo().getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoRescate_montoSuperaElUltimoMontoActualizado_lanzaExcepcion() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);

        assertThatThrownBy(() -> movimientoService.realizarMovimientoTipoRescate(activoLecaps.getId(), new BigDecimal("6000.00")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("El monto a rescatar supera el disponible");
    }

    @WithMockUser("username")
    @Test
    public void realizarMovimientoTipoRescate_montoARescatarIgualAlDisponible_lanzaExcepcion() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));
        BigDecimal montoIgual = new BigDecimal("5000.00");

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), montoIgual, activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);

        assertThatThrownBy(() -> movimientoService.realizarMovimientoTipoRescate(activoLecaps.getId(), montoIgual))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Seleccioná el rescate total.");
    }

    @WithMockUser("username")
    @Test
    public void buscarPorActivoId_existenMovimientos_retornaLosMovimientosParaElActivoOrdenadoPorFechaDescendente() {
        Activo unActivo = activoRepository.save(ActivoUtils.crearActivo("unActivo", ARS, usuario, null, false));

        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, INVERSION, usuario, false);
        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.now().plusDays(1), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, RESCATE, usuario, false);

        movimientoRepository.saveAll(Arrays.asList(movimientoInversion, movimientoActualizacion, movimientoRescate));

        List<MovimientoSalienteVo> movimientosObtenidos = movimientoService.buscarPorActivoId(unActivo.getId());

        assertThat(movimientosObtenidos.get(0)).isEqualToIgnoringGivenFields(convertirMovimientoAMovimientoSalienteVo(movimientoRescate), "id");
        assertThat(movimientosObtenidos.get(1)).isEqualToIgnoringGivenFields(convertirMovimientoAMovimientoSalienteVo(movimientoActualizacion), "id");
        assertThat(movimientosObtenidos.get(2)).isEqualToIgnoringGivenFields(convertirMovimientoAMovimientoSalienteVo(movimientoInversion), "id");

    }

    @WithMockUser("username")
    @Test
    public void buscarPorActivoId_conPrimerMovimientoActualizacionCantidadMovimientosMasQue10_retornaLosMovimientosSinCambiarElPrimeroAInversion() {
        Activo unActivo = activoRepository.save(ActivoUtils.crearActivo("unActivo", ARS, usuario, null, false));

        crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, ACTUALIZACION, usuario, false);
        crearMovimiento(LocalDateTime.now().plusDays(1), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, RESCATE, usuario, false);

        movimientoRepository
                .saveAll(Arrays.asList(
                        crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(1), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false),
                        crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, ACTUALIZACION, usuario, false)
                ));
        List<MovimientoSalienteVo> movimientosObtenidos = movimientoService.buscarPorActivoId(unActivo.getId());

        assertThat(movimientosObtenidos.get(9).getTipo()).isEqualTo("ACTUALIZACIÓN");
    }

    @WithMockUser("username")
    @Test
    public void buscarPorActivoId_conPrimerMovimientoActualizacionCantidadMovimientosMenoresQue10_retornaLosMovimientosConPrimerMovimientoCambiadoAInversion() {
        Activo unActivo = activoRepository.save(ActivoUtils.crearActivo("unActivo", ARS, usuario, null, false));

        Movimiento movimientoActualizacion = crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, ACTUALIZACION, usuario, false);
        Movimiento movimientoInversion = crearMovimiento(LocalDateTime.now().plusDays(1), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoRescate = crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, RESCATE, usuario, false);

        movimientoRepository.saveAll(Arrays.asList(movimientoInversion, movimientoActualizacion, movimientoRescate));

        List<MovimientoSalienteVo> movimientosObtenidos = movimientoService.buscarPorActivoId(unActivo.getId());

        assertThat(movimientosObtenidos.get(2).getTipo()).isEqualTo("INVERSIÓN");
    }

    @WithMockUser("username")
    @Test
    public void buscarPorActivoId_existenMovimientos_retornaLosUltimosDiezMovimientosParaElActivo() {
        Activo unActivo = activoRepository.save(ActivoUtils.crearActivo("unActivo", ARS, usuario, null, false));

        Movimiento movimientoUno = crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, INVERSION, usuario, false);
        Movimiento movimientoDos = crearMovimiento(LocalDateTime.now().plusDays(1), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoTres = crearMovimiento(LocalDateTime.now().plusDays(2), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoCuatro = crearMovimiento(LocalDateTime.now().plusDays(3), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoCinco = crearMovimiento(LocalDateTime.now().plusDays(4), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoSeis = crearMovimiento(LocalDateTime.now().plusDays(5), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoSiete = crearMovimiento(LocalDateTime.now().plusDays(6), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoOcho = crearMovimiento(LocalDateTime.now().plusDays(7), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoNueve = crearMovimiento(LocalDateTime.now().plusDays(8), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoDiez = crearMovimiento(LocalDateTime.now().plusDays(9), BigDecimal.TEN, unActivo, INVERSION, usuario, false);
        Movimiento movimientoOnce = crearMovimiento(LocalDateTime.now().plusDays(10), BigDecimal.TEN, unActivo, INVERSION, usuario, false);

        movimientoRepository
                .saveAll(Arrays.asList(
                        movimientoUno,
                        movimientoDos,
                        movimientoTres,
                        movimientoCuatro,
                        movimientoCinco,
                        movimientoSeis,
                        movimientoSiete,
                        movimientoOcho,
                        movimientoNueve,
                        movimientoDiez,
                        movimientoOnce));

        List<MovimientoSalienteVo> movimientosObtenidos = movimientoService.buscarPorActivoId(unActivo.getId());

        assertThat(movimientosObtenidos.size()).isEqualTo(10);
        assertThat(movimientosObtenidos.get(9)).isEqualToIgnoringGivenFields(convertirMovimientoAMovimientoSalienteVo(movimientoDos), "id");

    }

    @WithMockUser("username")
    @Test
    public void buscarPorActivoId_conMovimientoDeOtroUsuario_retornaLosMovimientosParaElUsuarioLogueado() {
        Usuario otroUsuario = guardarOtroUsuario();
        Activo unActivo = activoRepository.save(ActivoUtils.crearActivo("unActivo", ARS, usuario, null, false));

        Movimiento movimientoOtroUsuario = crearMovimiento(LocalDateTime.now(), BigDecimal.ONE, unActivo, INVERSION, otroUsuario, false);
        Movimiento movimientoUsuarioLogueado = crearMovimiento(LocalDateTime.now(), BigDecimal.TEN, unActivo, INVERSION, usuario, false);

        movimientoRepository.saveAll(Arrays.asList(movimientoOtroUsuario, movimientoUsuarioLogueado));

        List<MovimientoSalienteVo> movimientosObtenidos = movimientoService.buscarPorActivoId(unActivo.getId());

        assertThat(movimientosObtenidos.get(0)).isEqualToIgnoringGivenFields(convertirMovimientoAMovimientoSalienteVo(movimientoUsuarioLogueado), "id");

    }

}
