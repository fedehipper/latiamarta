package com.latiamarta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CriptoMonedaVo;
import com.latiamarta.domain.vo.CurrentPrice;
import com.latiamarta.domain.vo.Image;
import com.latiamarta.domain.vo.MarketData;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class CriptoMonedaServiceTest extends ApplicationTests {
    
    @Autowired
    private CriptoMonedaService criptoMonedaService;
    
    @Autowired
    private MockRestServiceServer mockRestServiceServer;
    
    @Test
    public void buscarCriptoMonedaIdPorSimbolo_simboloExistente_retornaListaCriptoMonedasParaElSimbolo() throws JsonProcessingException { 
        String simboloCriptoMoneda = "btc";      
          
        CriptoMonedaVo btc = new CriptoMonedaVo();
        btc.setId("bitcoin");
        btc.setSymbol(simboloCriptoMoneda);
        btc.setName("Bitcoin");
        btc.setImagen("url-imagen-large");
        
        CriptoMonedaVo ether = new CriptoMonedaVo();
        ether.setId("ether");
        ether.setSymbol("eth");
        ether.setName("Ether");
                
        List<CriptoMonedaVo> criptoMonedasEsperadas = Arrays.asList(btc, ether);
        
        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/list")
                .toUriString();

        ObjectMapper mapper = new ObjectMapper();
        String criptoMonedasEsperadasJson = mapper.writeValueAsString(criptoMonedasEsperadas);
        
        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(criptoMonedasEsperadasJson, MediaType.APPLICATION_JSON));
        
         mockCotizacionEImagenCriptomoneda();
        
        List<CriptoMonedaVo> criptomonedasPorSimboloEncontradas =  criptoMonedaService
                .buscarCriptoMonedaPorSimbolo(simboloCriptoMoneda);
        
        assertThat(criptoMonedasEsperadas.get(0)).isEqualToComparingFieldByField(criptomonedasPorSimboloEncontradas.get(0));
        assertThat(criptomonedasPorSimboloEncontradas.size()).isEqualTo(1);
    }
    
    private void mockCotizacionEImagenCriptomoneda() throws JsonProcessingException {
        String idBtc = "bitcoin";
        
        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/"+idBtc)
                .toUriString();
        
        CurrentPrice currentPrice = new CurrentPrice();
        currentPrice.setUsd("11.11");
        
        MarketData marketData = new MarketData();
        marketData.setCurrentPrice(currentPrice);
        
        Image image = new Image();
        image.setLarge("url-imagen-large");
        
        CotizacionCriptoMonedaVo cotizacionCriptomonedaVo = new CotizacionCriptoMonedaVo();
        cotizacionCriptomonedaVo.setMarketPrice(marketData);
        cotizacionCriptomonedaVo.setImage(image);
        
        ObjectMapper mapper = new ObjectMapper();
        String cotizacionEsperadaJson = mapper.writeValueAsString(cotizacionCriptomonedaVo);

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(cotizacionEsperadaJson, MediaType.APPLICATION_JSON));

    }
    
    
}
