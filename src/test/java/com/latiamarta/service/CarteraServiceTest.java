package com.latiamarta.service;

import com.latiamarta.util.ActivoUtils;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.Porcentaje;
import com.latiamarta.domain.TipoMoneda;
import java.math.BigDecimal;
import java.time.Month;
import org.springframework.beans.factory.annotation.Autowired;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.MovimientoRepository;
import static com.latiamarta.util.MovimientoUtils.crearMovimiento;
import java.io.IOException;
import java.util.List;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import com.latiamarta.domain.Usuario;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Set;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

@AutoConfigureMockRestServiceServer
public class CarteraServiceTest extends ApplicationTests {

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private CarteraService carteraService;

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @Value("${com.latiamarta.cotizacion.dolar.url}")
    private String urlCotizacion;

    @WithMockUser("username")
    @Test
    public void obtenerTipoMonedasDisponiblesEnCartera_activosTipoArsUnicamente_retornaListaConArs() {
        Usuario otroUsuario = guardarOtroUsuario();
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.USD, otroUsuario, null, false));
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));
        activoRepository.save(ActivoUtils.crearActivo("OTRO", TipoMoneda.ARS, usuario, null, false));
        Set<TipoMoneda> tipoMonedasDisponibles = carteraService.obtenerTipoMonedasDisponiblesEnCartera();

        assertThat(tipoMonedasDisponibles).isEqualTo(Set.of(TipoMoneda.ARS));
        assertThat(tipoMonedasDisponibles.size()).isEqualTo(1);
    }

    @WithMockUser("username")
    @Test
    public void obtenerTipoMonedasDisponiblesEnCartera_activosTipoUsdUnicamente_retornaListaConArs() {
        activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.USD, usuario, null, false));
        Set<TipoMoneda> tipoMonedasDisponibles = carteraService.obtenerTipoMonedasDisponiblesEnCartera();

        assertThat(tipoMonedasDisponibles).isEqualTo(Set.of(TipoMoneda.USD));
    }

    @WithMockUser("otroUsuario")
    @Test
    public void obtenerCartera_conUsuarioNoPuedeVerCarterasDeOtroUsuario_retornaVacio() {
        Activo activo = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));
        Movimiento movimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 5, 0, 0), new BigDecimal("2000.00"), activo, ACTUALIZACION, usuario, false);

        movimientoRepository.save(movimiento);

        List<DetalleActivo> cartera = carteraService.obtenerCartera();

        assertTrue(cartera.isEmpty());
    }

    @WithMockUser("username")
    @Test
    public void obtenerCartera_conMovimientosExistentes_retornaCarteraConActivosDetallados() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));
        Activo activoMacro = activoRepository.save(ActivoUtils.crearActivo("MACRO", null, usuario, null, false));

        Movimiento movimientoRentaVariable = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 5, 0, 0), new BigDecimal("2000.00"), activoMacro, ACTUALIZACION, usuario, false);
        Movimiento movimientoInicialRentaFija = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoRentaFija = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 14, 0, 0), new BigDecimal("5100.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoUltimoRentaFija = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 16, 0, 0), new BigDecimal("5200.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(movimientoInicialRentaFija);
        movimientoRepository.save(otroMovimientoRentaFija);
        movimientoRepository.save(movimientoUltimoRentaFija);
        movimientoRepository.save(movimientoRentaVariable);

        List<DetalleActivo> cartera = carteraService.obtenerCartera();

        assertThat(cartera.get(0).getId()).isEqualTo(activoLecaps.getId());
        assertThat(cartera.get(1).getId()).isEqualTo(activoMacro.getId());
        assertThat(cartera.size()).isEqualTo(2);
        assertThat(cartera.get(0).getNombre()).isEqualTo("LECAPS");
        assertThat(cartera.get(0).getMontoActual()).isEqualTo(new BigDecimal("5200.00"));
        assertThat(cartera.get(0).getGananciaParcial()).isEqualTo(new BigDecimal("100.00"));
        assertThat(cartera.get(0).getGananciaTotal()).isEqualTo(new BigDecimal("200.00"));
    }

    @WithMockUser("username")
    @Test
    public void obtenerCartera_conActivosInexistentes_retornaCarteraVacia() {
        List<DetalleActivo> cartera = carteraService.obtenerCartera();

        assertTrue(cartera.isEmpty());
    }

    @WithMockUser("otroUsuario")
    @Test
    public void obtenerPorcentaje_activoDolarizadoYEnPesosNoSonDeUsuarioLogueado_porcentajesVacioPorSerOtroUsuarioElLogueado() throws IOException {
        String uri = UriComponentsBuilder.fromHttpUrl("http://ws.geeklab.com.ar")
                .pathSegment("dolar")
                .pathSegment("get-dolar-json.php")
                .toUriString();

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"libre\":\"10.00\"}", MediaType.TEXT_PLAIN));

        activoRepository.save(ActivoUtils.crearActivo("LECAP", TipoMoneda.ARS, usuario, null, false));
        activoRepository.save(ActivoUtils.crearActivo("LEBAC", TipoMoneda.ARS, usuario, null, false));
        activoRepository.save(ActivoUtils.crearActivo("LETE", TipoMoneda.USD, usuario, null, false));

        List<Porcentaje> porcentajes = carteraService.obtenerPorcentaje();

        assertTrue(porcentajes.isEmpty());
    }

    @WithMockUser("username")
    @Test
    public void obtenerPorcentaje_activoDolarizadoYEnPesos_porcentajes() throws IOException {
        String uri = UriComponentsBuilder.fromHttpUrl(urlCotizacion)
                .toUriString();

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("{\"venta\":\"59.51\"}", MediaType.TEXT_PLAIN));

        Activo activoPesos = ActivoUtils.crearActivo("LECAP", TipoMoneda.ARS, usuario, null, false);
        Activo activoDolares = ActivoUtils.crearActivo("LETE", TipoMoneda.USD, usuario, null, false);

        activoRepository.save(activoPesos);
        activoRepository.save(activoDolares);

        Movimiento movimientoActivoEnDolares = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 5, 0, 0), new BigDecimal("2000.00"), activoDolares, ACTUALIZACION, usuario, false);
        Movimiento movimientoActivoEnPesos = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5000.00"), activoPesos, ACTUALIZACION, usuario, false);

        movimientoRepository.save(movimientoActivoEnPesos);
        movimientoRepository.save(movimientoActivoEnDolares);

        List<Porcentaje> porcentajesEntreActivos = carteraService.obtenerPorcentaje();

        assertThat(porcentajesEntreActivos.get(0).getNombreActivo()).isEqualTo("LECAP");
        assertThat(porcentajesEntreActivos.get(0).getPorcentajeActivo()).isEqualTo(new BigDecimal("4.03"));

        assertThat(porcentajesEntreActivos.get(1).getNombreActivo()).isEqualTo("LETE");
        assertThat(porcentajesEntreActivos.get(1).getPorcentajeActivo()).isEqualTo(new BigDecimal("95.96"));
    }

    @WithMockUser("username")
    @Test
    public void obtenerPorcentaje_ActivosInexistentes_retornaNull() throws IOException {
        List<Porcentaje> porcentajesEntreActivos = carteraService.obtenerPorcentaje();
        assertTrue(porcentajesEntreActivos.isEmpty());
    }

    @Test
    public void obtenerTipoMonedas_existen_listaDeTipoMonedas() {
        List<TipoMoneda> tipoMonedas = carteraService.obtenerTipoMonedasNuevaInversion();
        assertThat(tipoMonedas).isEqualTo(Arrays.asList(TipoMoneda.valueOf("ARS"), TipoMoneda.valueOf("USD")));
    }

}
