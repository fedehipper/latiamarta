package com.latiamarta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.latiamarta.util.ActivoUtils;
import com.latiamarta.ApplicationTests;
import com.latiamarta.domain.Activo;
import com.latiamarta.domain.DetalleActivo;
import com.latiamarta.domain.DetalleTotal;
import com.latiamarta.domain.Movimiento;
import com.latiamarta.domain.TipoMoneda;
import static com.latiamarta.domain.TipoMovimiento.INVERSION;
import static com.latiamarta.domain.TipoMovimiento.RESCATE;
import java.math.BigDecimal;
import java.time.Month;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import com.latiamarta.repository.ActivoRepository;
import com.latiamarta.repository.MovimientoRepository;
import static com.latiamarta.util.MovimientoUtils.crearMovimiento;
import org.springframework.security.test.context.support.WithMockUser;
import static com.latiamarta.domain.TipoMovimiento.ACTUALIZACION;
import com.latiamarta.domain.Usuario;
import com.latiamarta.domain.vo.CotizacionCriptoMonedaVo;
import com.latiamarta.domain.vo.CurrentPrice;
import com.latiamarta.domain.vo.MarketData;
import java.time.LocalDateTime;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockRestServiceServer
public class DetalleActivoServiceTest extends ApplicationTests {

    @Autowired
    private DetalleActivoService detalleActivoService;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_MultiplesMovimientosDeVariasMonedas_retornaDetalle() {
        Usuario otroUsuario = guardarOtroUsuario();
        Activo activoOtroUsuario = activoRepository.save(ActivoUtils.crearActivo("LECAP", TipoMoneda.ARS, otroUsuario, null, false));
        Movimiento unMovimientoOtroUsuario = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activoOtroUsuario, ACTUALIZACION, otroUsuario, false);
        movimientoRepository.save(unMovimientoOtroUsuario);

        Activo lecap = activoRepository.save(ActivoUtils.crearActivo("LECAP", TipoMoneda.ARS, usuario, null, false));
        Activo lete = activoRepository.save(ActivoUtils.crearActivo("LETE", TipoMoneda.USD, usuario, null, false));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), lecap, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), lete, ACTUALIZACION, usuario, false);
        Movimiento inversionRegalo = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), lete, INVERSION, usuario, true);
        Movimiento inversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("10.00"), lete, INVERSION, usuario, false);
        Movimiento unMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("11000.00"), lecap, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 14, 0, 0), new BigDecimal("7000.00"), lete, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(unMovimientoOtraFecha);
        movimientoRepository.save(inversionRegalo);
        movimientoRepository.save(inversion);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(otroMovimientoOtraFecha);

        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();

        DetalleTotal detalleUsd = detalleActivo.stream().filter(detalle -> TipoMoneda.USD.equals(detalle.getTipoMoneda())).findFirst().get();
        DetalleTotal detalleArs = detalleActivo.stream().filter(detalle -> TipoMoneda.ARS.equals(detalle.getTipoMoneda())).findFirst().get();

        assertThat(detalleArs.getTipoMoneda()).isEqualTo(TipoMoneda.ARS);
        assertThat(detalleArs.getMontoActual()).isEqualTo(new BigDecimal("11000.00"));
        assertThat(detalleArs.getGananciaParcial()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(detalleArs.getGananciaTotal()).isEqualTo(new BigDecimal("1000.00"));

        assertThat(detalleUsd.getTipoMoneda()).isEqualTo(TipoMoneda.USD);
        assertThat(detalleUsd.getMontoActual()).isEqualTo(new BigDecimal("7000.00"));
        assertThat(detalleUsd.getGananciaParcial()).isEqualTo(new BigDecimal("2000.00"));
        assertThat(detalleUsd.getGananciaTotal()).isEqualTo(new BigDecimal("1990.00"));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_unaUltimaFechaActivo_retornaDetalle() {

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));
        Activo activoLete = activoRepository.save(ActivoUtils.crearActivo("LETE", TipoMoneda.ARS, usuario, null, false));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLete, ACTUALIZACION, usuario, false);
        Movimiento unMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("11000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 14, 0, 0), new BigDecimal("7000.00"), activoLete, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(unMovimientoOtraFecha);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(otroMovimientoOtraFecha);

        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();

        assertThat(detalleActivo.get(0).getTipoMoneda()).isEqualTo(TipoMoneda.ARS);
        assertThat(detalleActivo.get(0).getMontoActual()).isEqualTo(new BigDecimal("18000.00"));
        assertThat(detalleActivo.get(0).getGananciaParcial()).isEqualTo(new BigDecimal("3000.00"));
        assertThat(detalleActivo.get(0).getGananciaTotal()).isEqualTo(new BigDecimal("3000.00"));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_dosMovientosUltimaFechas_retornaDetalle() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));
        Activo activoLete = activoRepository.save(ActivoUtils.crearActivo("LETE", TipoMoneda.ARS, usuario, null, false));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 14, 0, 0), new BigDecimal("11000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLete, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoOtraFecha = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 14, 0, 0), new BigDecimal("7000.00"), activoLete, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(unMovimientoOtraFecha);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(otroMovimientoOtraFecha);

        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();
        assertThat(detalleActivo.get(0).getMontoActual()).isEqualTo(new BigDecimal("18000.00"));
        assertThat(detalleActivo.get(0).getGananciaParcial()).isEqualTo(new BigDecimal("3000.00"));
        assertThat(detalleActivo.get(0).getGananciaTotal()).isEqualTo(new BigDecimal("3000.00"));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_sinActivos_retornaNull() {
        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();
        assertTrue(detalleActivo.isEmpty());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_sinActivosParaUsuarioLogueado_retornaNull() {
        Usuario otroUsuario = guardarOtroUsuario();
        Activo activoOtroUsuario = activoRepository.save(ActivoUtils.crearActivo("LECAP", TipoMoneda.ARS, otroUsuario, null, false));
        Movimiento unMovimientoOtroUsuario = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 10, 0, 0), new BigDecimal("10000.00"), activoOtroUsuario, ACTUALIZACION, otroUsuario, false);
        movimientoRepository.save(unMovimientoOtroUsuario);

        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();
        assertTrue(detalleActivo.isEmpty());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleMovimientoTotal_seCalculaElDetalleConSoloUnMovimiento_seTomaComoMontoAnteriorCero() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));
        movimientoRepository.save(crearMovimiento(LocalDateTime.now(), new BigDecimal("10.00"), activoLecaps, ACTUALIZACION, usuario, false));

        List<DetalleTotal> detalleActivo = detalleActivoService.calcularDetalleMovimientoTotal();

        assertThat(detalleActivo.get(0).getMontoActual()).isEqualTo(BigDecimal.TEN.setScale(2));
        assertThat(detalleActivo.get(0).getGananciaParcial()).isEqualTo(BigDecimal.ZERO.setScale(2));
        assertThat(detalleActivo.get(0).getGananciaTotal()).isEqualTo(BigDecimal.ZERO.setScale(2));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientosConCotizacion_retornaElDetalleDeEsteActivoConValorMedio() throws JsonProcessingException {
        mockCotizacionCriptomoneda();

        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, "bitcoin", true));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        otroMovimientoVigente.setCotizacionActivo(BigDecimal.TEN);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(otroMovimientoVigente);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMoneda()).isEqualTo(TipoMoneda.ARS);
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("10000.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(new BigDecimal("5000.00"));
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("5000.00"));
        assertThat(detalleActivo.getValorMedio()).isEqualTo(new BigDecimal("10.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
        assertThat(detalleActivo.isCriptomoneda()).isTrue();
    }
    
    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conTipoCriptoMonedaYNoEsPrimeraInversionEnCriptomoneda_noSeCalculaValorMedio() throws JsonProcessingException {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, "bitcoin", false));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        otroMovimientoVigente.setCotizacionActivo(BigDecimal.TEN);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(otroMovimientoVigente);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getValorMedio()).isNull();
    }
    
    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_activoCripto_retornaDetalleConImagenUrl() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));
        activoLecaps.setCriptomonedaImagenUrl("criptomoneda-imagen-url");
        
        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimientoVigente);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(activoLecaps.getCriptomonedaImagenUrl()).isEqualTo(detalleActivo.getCriptomonedaImagenUrl());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientosDeUnTipoExistentes_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", TipoMoneda.ARS, usuario, null, false));

        Movimiento unMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimientoVigente);
        movimientoRepository.save(otroMovimientoVigente);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getValorMedio()).isNull();
        assertThat(detalleActivo.isCriptomoneda()).isFalse();
    }

    private void mockCotizacionCriptomoneda() throws JsonProcessingException {
        String idBtc = "bitcoin";

        String uri = UriComponentsBuilder.fromHttpUrl("https://api.coingecko.com/api/v3/coins/" + idBtc)
                .toUriString();

        CurrentPrice currentPrice = new CurrentPrice();
        currentPrice.setUsd("11.11");

        MarketData marketData = new MarketData();
        marketData.setCurrentPrice(currentPrice);

        CotizacionCriptoMonedaVo cotizacionCriptomonedaVo = new CotizacionCriptoMonedaVo();
        cotizacionCriptomonedaVo.setMarketPrice(marketData);

        ObjectMapper mapper = new ObjectMapper();
        String cotizacionEsperadaJson = mapper.writeValueAsString(cotizacionCriptomonedaVo);

        mockRestServiceServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(cotizacionEsperadaJson, MediaType.APPLICATION_JSON));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoTipoInversion_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoInversion = crearMovimiento(LocalDateTime.now(), new BigDecimal("1000.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoTipoInversion);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("11000.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(new BigDecimal("5000.00"));
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("5000.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoDiarioLuegoDeInversionVariacionParcialCero_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("30894.73"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, INVERSION, usuario, false);
        Movimiento movimientoTipoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("40961.70"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.now(), new BigDecimal("5000.00"), activoLecaps, RESCATE, usuario, false);

        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(movimientoTipoRescate);
        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoTipoInversion);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoDiarioLuegoDeRescateVariacionParcialCero_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("30894.73"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("10000.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento movimientoTipoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("40961.70"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.now(), new BigDecimal("5000.00"), activoLecaps, RESCATE, usuario, false);

        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(movimientoTipoRescate);
        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoTipoInversion);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoTipoInversionVariosYDiario_retornaGananciaTotalEnDetalleContandoConTodasLasInversiones() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento primerMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5000.00"), activoLecaps, INVERSION, usuario, true);
        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("17000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoInversion = crearMovimiento(LocalDateTime.now(), new BigDecimal("1000.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(primerMovimiento);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoTipoInversion);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("7000.00"));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoTipoRescateVariosYDiario_retornaGananciaTotalEnDetalleContandoConTodasLosRescates() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento primerMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("10000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5000.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("7000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.now(), new BigDecimal("1000.00"), activoLecaps, RESCATE, usuario, false);

        movimientoRepository.save(primerMovimiento);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(movimientoTipoRescate);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("2000.00"));
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conUnDiarioYUnInversion_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("1000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.now(), new BigDecimal("100.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(otroMovimientoVigente);
        movimientoRepository.save(unMovimiento);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("1100.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("0.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conUnDiarioYMultiplesInversiones_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("1000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("100.00"), activoLecaps, INVERSION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.now(), new BigDecimal("100.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(otroMovimientoVigente);
        movimientoRepository.save(unMovimientoInversion);
        movimientoRepository.save(unMovimiento);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("1200.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("0.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conUnDiarioYUnRescate_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("1000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.now(), new BigDecimal("100.00"), activoLecaps, RESCATE, usuario, false);

        movimientoRepository.save(otroMovimientoVigente);
        movimientoRepository.save(unMovimiento);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("900.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("0.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conUnDiarioYMultiplesRescate_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("1000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("100.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.now(), new BigDecimal("100.00"), activoLecaps, RESCATE, usuario, false);

        movimientoRepository.save(otroMovimientoVigente);
        movimientoRepository.save(unMovimientoInversion);
        movimientoRepository.save(unMovimiento);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("800.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("0.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conUnDiarioYMultiplesMovimientosDistintosDeDiario_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("1000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento unMovimientoInversion = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("100.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento otroMovimientoVigente = crearMovimiento(LocalDateTime.now(), new BigDecimal("100.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(otroMovimientoVigente);
        movimientoRepository.save(unMovimientoInversion);
        movimientoRepository.save(unMovimiento);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("0.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoTipoRescateYHoyDiario_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("1000.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.now(), new BigDecimal("6000.00"), activoLecaps, ACTUALIZACION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(movimientoTipoRescate);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("6000.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(BigDecimal.ZERO);
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("2000.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_conMovimientoTipoDiarioYLosMasRecientesRescateEInversion_retornaElDetalleDeEsteActivo() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        Movimiento unMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 11, 0, 0), new BigDecimal("4000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento otroMovimiento = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 12, 0, 0), new BigDecimal("5000.00"), activoLecaps, ACTUALIZACION, usuario, false);
        Movimiento movimientoTipoRescate = crearMovimiento(LocalDateTime.of(2018, Month.MAY, 13, 0, 0), new BigDecimal("1000.00"), activoLecaps, RESCATE, usuario, false);
        Movimiento otroMovimientoInversion = crearMovimiento(LocalDateTime.now(), new BigDecimal("6000.00"), activoLecaps, INVERSION, usuario, false);

        movimientoRepository.save(unMovimiento);
        movimientoRepository.save(otroMovimientoInversion);
        movimientoRepository.save(otroMovimiento);
        movimientoRepository.save(movimientoTipoRescate);

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo.getNombre()).isEqualTo("LECAPS");
        assertThat(detalleActivo.getMontoActual()).isEqualTo(new BigDecimal("10000.00"));
        assertThat(detalleActivo.getGananciaParcial()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(detalleActivo.getGananciaTotal()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(detalleActivo.getId()).isEqualTo(activoLecaps.getId());
    }

    @WithMockUser("username")
    @Test
    public void calcularDetalleActivoIndividual_sinMovimientos_retornaNull() {
        Activo activoLecaps = activoRepository.save(ActivoUtils.crearActivo("LECAPS", null, usuario, null, false));

        DetalleActivo detalleActivo = detalleActivoService.calcularDetalleActivoIndividual(activoLecaps.getId());

        assertThat(detalleActivo).isNull();
    }

}
